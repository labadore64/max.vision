﻿using AccessibilityDemo.demo.player;
using max.geometry.shape2D;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace AccessibilityDemo.demo.test
{
    public class TestCircle
    {
        Circle2D Circle = new Circle2D();

        public float Radius
        { 
            get
            {
                return Circle.Radius;
            }
            set
            {
                Circle.Radius = value;
            }
        }
        public Vector2 Center
        {
            get
            {
                return Circle.Center;
            }
            set
            {
                Circle.Center = value;
            }
        }
        public List<Vector2> Intersection { get; set; } = new List<Vector2>();

        public TestCircle()
        {
            Radius = 100;
            Center = new Vector2(400, 250);
        }

        public void Update()
        {
            Intersection = Circle.LineIntersectClosestPoint(Player.Position, Player.LookVector);
        }

        public void Draw(SpriteBatch spriteBatch, GraphicsDevice GD)
        {
            DrawDebug(
                2,
                2,
                Center,

                spriteBatch, GD, Color.Black);
            for(int i = 0; i < Intersection.Count; i++)
            {

                DrawDebug(
                    4,
                    4,
                    Intersection[i],

                    spriteBatch, GD, Color.Blue);
            }
        }

        private static void DrawDebug(int width, int height, Vector2 pos, SpriteBatch spriteBatch, GraphicsDevice GD, Color c)
        {
            int left = (int)pos.X;
            int top = (int)pos.Y;
            if (spriteBatch != null)
            {
                Color[] data = new Color[width * height];
                Texture2D rectTexture = new Texture2D(GD, width, height);

                for (int i = 0; i < data.Length; ++i)
                    data[i] = Color.White;

                rectTexture.SetData(data);
                var position = new Vector2(left - width * .5f, top - height * .5f);

                spriteBatch.Draw(rectTexture, position, c);
            }

        }
    }
}
