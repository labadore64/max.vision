﻿using AccessibilityDemo.demo.map.component;
using max.vision;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace AccessibilityDemo.demo.map
{
    public class Map
    {
        [XmlArray("Collisions")]
        [XmlArrayItem("Collision")]
        public List<CollisionData> Collisions = new List<CollisionData>();

        public SpawnData Spawn = new SpawnData();

        [XmlArray("WarpAreas")]
        [XmlArrayItem("WarpArea")]
        public List<WarpEnterData> WarpAreas = new List<WarpEnterData>();

        [XmlArray("WarpDestination")]
        [XmlArrayItem("WarpDestination")]
        public List<WarpDestinationData> WarpDestinations = new List<WarpDestinationData>();

        [XmlArray("AXLabels")]
        [XmlArrayItem("AXLabel")]
        public List<AXLabelData> AXLabels = new List<AXLabelData>();

        [XmlArray("Sounds")]
        [XmlArrayItem("Sound")]
        public List<SoundEffectData> Sounds = new List<SoundEffectData>();


        [XmlArray("AXAreaLabels")]
        [XmlArrayItem("AXAreaLabel")]
        public List<AXLabelAreaData> AXLabelAreas = new List<AXLabelAreaData>();

        [XmlAttribute]
        public String Name { get; set; }

        public void LoadMap()
        {
            MaxVision.MapReset();
            OverworldMap.Clear();
            OverworldMap.Map = this;

            foreach(CollisionData c in Collisions)
            {
                c.Generate();
            }

            foreach(AXLabelData l in AXLabels)
            {
                l.Generate();
            }

            foreach(AXLabelAreaData l in AXLabelAreas)
            {
                l.Generate();
            }

            foreach (SoundEffectData l in Sounds)
            {
                l.Generate();
            }

            foreach (WarpEnterData l in WarpAreas)
            {
                l.Generate();
            }
        }

        public WarpDestinationData GetDestinationFromName(String name)
        {
            return WarpDestinations.Find(x => x.ID.Equals(name));
        }

        public void SaveBookmarks()
        {
            MaxVision.SaveBookmarks(Name);
        }

        public void LoadBookmarks()
        {
            MaxVision.LoadBookmarks(Name);
        }

    }
}
