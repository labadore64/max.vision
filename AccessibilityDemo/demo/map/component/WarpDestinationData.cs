﻿using Microsoft.Xna.Framework;
using System;
using System.Xml.Serialization;

namespace AccessibilityDemo.demo.map.component
{
    public class WarpDestinationData
    {
        public Vector2 Position { get; set; }
        [XmlAttribute]
        public String ID { get; set; } = "";
    }
}
