﻿using AccessibilityDemo.demo.player;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AccessibilityDemo.demo.map.component
{
    public class WarpEnterData : IComponent
    {
        [XmlArray("Shape")]
        [XmlArrayItem("Point")]
        public Vector2[] Shape { get; set; }

        public String Map { get; set; }
        public String Destination { get; set; }
        

        public void Generate()
        {
            Warp w = new Warp(Shape);
            w.DestinationMap = DataContainer.GetMapFromName(Map);
            w.Destination = DataContainer.GetDestinationFromMapAndName(Map, Destination);
            OverworldMap.Warps.Add(w);
        }
    }
}
