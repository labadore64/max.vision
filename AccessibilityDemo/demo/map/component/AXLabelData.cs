﻿using max.vision;
using max.vision.stage.component.label;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AccessibilityDemo.demo.map.component
{
    public class AXLabelData : IComponent
    {
        public Vector2 Position { get; set; }

        [XmlAttribute]
        public float Radius { get; set; }

        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public String Description { get; set; }

        public void Generate()
        {
            AXLabel label = MaxVision.AddAXLabel(Name, Description, Radius, Position);
        }
    }
}
