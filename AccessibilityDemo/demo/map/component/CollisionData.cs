﻿using AccessibilityDemo.demo.player;
using max.vision;
using max.geometry.shape2D;
using Microsoft.Xna.Framework;
using System.Xml.Serialization;
using max.vision.stage.component;

namespace AccessibilityDemo.demo.map.component
{
    public class CollisionData : IComponent
    {
        [XmlArray("Shape")]
        [XmlArrayItem("Point")]
        public Vector2[] Shape { get; set; }

        public void Generate()
        {
            Polygon2D p = new Polygon2D(Shape);
            Collision c = new Collision(Shape);
            OverworldMap.Collisions.Add(c);
            MaxVision.MapAddCollision(new MapCollision(p));
        }
    }
}
