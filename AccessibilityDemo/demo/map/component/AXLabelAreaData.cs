﻿
using max.vision;
using max.geometry.shape2D;
using Microsoft.Xna.Framework;
using System;
using System.Xml.Serialization;
using max.vision.stage.component.label;

namespace AccessibilityDemo.demo.map.component
{
    public class AXLabelAreaData : IComponent
    {
        [XmlArray("Shape")]
        [XmlArrayItem("Point")]
        public Vector2[] Shape { get; set; }

        [XmlAttribute]
        public String Name { get; set; }
        [XmlAttribute]
        public String Description { get; set; }

        public void Generate()
        {
            AXLabel label = new AXLabel();
            label.Name = Name;
            label.Description = Description;
            label.Shape = new Polygon2D(Shape);
            label.MenuAllowed = false;
            MaxVision.AddAXLabel(label);
        }
    }
}
