﻿using max.vision;
using max.sound.emitter;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace AccessibilityDemo.demo.map.component
{
    public class SoundEffectData :IComponent
    {
        [XmlIgnore]
        public static List<String> SoundEmitterCollisions = new List<String>();

        public Vector2 Position { get; set; }
        [XmlAttribute]
        public float Radius { get; set; }
        [XmlAttribute]
        public String Filename { get; set; }

        public static void DoList()
        {
            SoundEmitterCollisions.Add("door");
            SoundEmitterCollisions.Add("shower");
            SoundEmitterCollisions.Add("sink");
            SoundEmitterCollisions.Add("tv");
        }

        public void Generate()
        {
            SoundEmitter x = (SoundEmitter)MaxVision.AddSoundEmitter(MaxVision.CreateSoundInstance(Filename), Position,Radius);

            x.CollisionChecking = false;

            for (int i = 0; i < SoundEmitterCollisions.Count; i++)
            {
                if (x.SoundEffect.Name.Contains(SoundEmitterCollisions[i]))
                {
                    x.CollisionChecking = true;
                }
            }

            x.Play();
        }
    }
}
