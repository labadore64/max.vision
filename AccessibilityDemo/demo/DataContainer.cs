﻿using AccessibilityDemo.demo.map;
using AccessibilityDemo.demo.map.component;
using max.vision;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;

namespace AccessibilityDemo.demo
{

    public static class DataContainer
    {
        static List<Map> MapList = new List<Map>();
        static Map _map;
        public static Map CurrentMap {
            get
            {
                return _map;
            }
            set
            {
                LoadMap(value);
            }
                
        }

        public static void LoadMapsFromFile(String filename)
        {
            TextReader reader = new StreamReader(@filename);
            try
            {
                XmlSerializer serial = new XmlSerializer(MapList.GetType());
                MapList = (List<Map>)serial.Deserialize(reader);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                reader.Close();
            }
        }

        public static void LoadMap(int index)
        {
            LoadMap(MapList[index]);
        }

        public static void LoadMap(String MapName)
        {
            LoadMap(MapList.Find(m => m.Name.Equals(MapName)));
        }

        public static void LoadMap(Map map)
        {
            if(map != null)
            {
                if (MapList.Contains(map))
                {
                    if (_map != null)
                    {
                        _map.SaveBookmarks();
                    }
                    map.LoadMap();
                    if (map != null)
                    {
                        MaxVision.ClearBookmarks();
                        map.LoadBookmarks();
                    }
                    _map = map;
                    MaxVision.Speak("Entered " + map.Name);
                    MaxVision.SetPathMap(10, 1000, 800);
                } else
                {
                    throw new Exception("I don't recognize that map.");
                }
            } else
            {
                throw new Exception("Map is null.");
            }

            MaxVision.MapController.WriteLayers();
        }

        public static WarpDestinationData GetDestinationFromMapAndName(String map, String name)
        {
            Map m = GetMapFromName(map);
            if(m != null)
            {
                return m.GetDestinationFromName(name);
            }

            return null;
        }

        public static Map GetMapFromName(String map)
        {
            return MapList.Find(z => z.Name.Equals(map));
        }
    }
}
