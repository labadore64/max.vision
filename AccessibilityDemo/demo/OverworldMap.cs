﻿using AccessibilityDemo.demo.map;
using AccessibilityDemo.demo.player;
using System.Collections.Generic;

namespace AccessibilityDemo.demo
{
    public static class OverworldMap
    {
        public static List<Collision> Collisions = new List<Collision>();
        public static List<Warp> Warps = new List<Warp>();
        public static Map Map;

        public static void Clear()
        {
            Collisions.Clear();
            Warps.Clear();
            Map = null;
        }
    }
}
