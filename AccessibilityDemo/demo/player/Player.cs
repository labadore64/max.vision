﻿using max.geometry.helper;
using max.input.container.keyboard;
using max.input.group;
using max.input.handler.keyboard;
using max.vision;
using max.vision.stage.component;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace AccessibilityDemo.demo.player
{
    public class Player
    {
        public static bool Lock = false;
        static GraphicsDeviceManager graphics;
        static SpriteBatch spriteBatch;
        static public Vector2 Position
        {
            get
            {
                return MaxVision.Player.Position;
            }
            set
            {
                MaxVision.Player.Position = value;
            }
        }
        static public Vector2 LookVector
        {
            get
            {
                return MaxVision.Player.LookVector;
            }
            set
            {
                MaxVision.Player.LookVector = value;
            }
        }

        static float Speed;
        static int DrawHeight = 10;
        static int DrawWidth = 10;

        const float ROTATE_AMOUNT = .05f;
        const float DIST_AMOUNT = 2f;
        public static List<Collision> CollisionList { get { return OverworldMap.Collisions; } }

        public static void Initialize(GraphicsDeviceManager m, SpriteBatch s)
        {
            graphics = m;
            spriteBatch = s;
            Position = Vector2.Zero;
            Speed = 2;
        }

        public static void Update()
        {
            if (!Lock)
            {

                if (MaxVision.MenuController.MenuStack.Count == 0)
                {
                    UpdateInputState();
                }
            }
        }

        private static void UpdateInputState()
        {
            if (MaxVision.InputController.GetInputPressed("test"))
            {
                InputGroup group = MaxVision.InputController.Groups["BasicInput"];
                LinkedKeyboardInputContainer key = (LinkedKeyboardInputContainer)group.GetInputContainer(typeof(KeyboardHandler), "linkedpress");
                Console.WriteLine("triggered");

                key.Activate();
            } else if (MaxVision.InputController.GetInputPressed("linkedpress"))
            {
                Console.WriteLine("hello world");
            }
            else if (MaxVision.InputController.GetInputReleased("linkedpress"))
            {
                Console.WriteLine("You missed!");
            } else if (MaxVision.InputController.GetInputHold("clockwise"))
            {
                MaxVision.Player.LookAngle += ROTATE_AMOUNT;
            }
            else if (MaxVision.InputController.GetInputHold("counterclockwise"))
            {
                MaxVision.Player.LookAngle -= ROTATE_AMOUNT;
            }
            else if (MaxVision.InputController.GetInputHold("zoomin"))
            {

                MaxVision.SoundController.LookDistance -= DIST_AMOUNT;
                    if(MaxVision.Player.LookDistance < 0)
                {
                    MaxVision.Player.LookDistance = 0;
                }
            }
            if (MaxVision.InputController.GetInputHold("zoomout"))
            {

                MaxVision.Player.LookDistance += DIST_AMOUNT;
                if (MaxVision.Player.LookDistance > 200)
                {
                    MaxVision.Player.LookDistance = 200;
                }
            }
            else if(MaxVision.InputController.GetInputPressed("select"))
            {
                MaxVision.OpenBookmarkMenu();
            } else if (MaxVision.InputController.GetInputPressed("cancel")){
                MaxVision.AddBookmark();
            }
            else if (MaxVision.InputController.GetInputPressed("coords"))
            {
                MaxVision.SpeakCoordinates2D();
            }
            else
            {
                //directional stuff
                Vector2 TestPos = Vector2Helper.DUMMY;
                double angle = 0;
                if (MaxVision.InputController.GetInputHold("up"))
                {
                    angle = Math.PI;
                    TestPos = new Vector2((float)(Position.X- Speed * Math.Cos(angle+MaxVision.Player.LookAngle)), (float)(Position.Y + Speed * Math.Sin(angle + MaxVision.Player.LookAngle)));

                } else if (MaxVision.InputController.GetInputHold("down"))
                {
                    TestPos = new Vector2((float)(Position.X - Speed * Math.Cos(angle + MaxVision.Player.LookAngle)), (float)(Position.Y + Speed * Math.Sin(angle + MaxVision.Player.LookAngle)));
                }
                else if (MaxVision.InputController.GetInputHold("left"))
                {
                    angle = Math.PI*1.5;
                    TestPos = new Vector2((float)(Position.X - Speed * Math.Cos(angle + MaxVision.Player.LookAngle)), (float)(Position.Y + Speed * Math.Sin(angle + MaxVision.Player.LookAngle)));
                }
                else if (MaxVision.InputController.GetInputHold("right"))
                {
                    angle = Math.PI * .5;
                    TestPos = new Vector2((float)(Position.X - Speed * Math.Cos(angle + MaxVision.Player.LookAngle)), (float)(Position.Y + Speed * Math.Sin(angle + MaxVision.Player.LookAngle)));
                }

                if (TestPos != Vector2Helper.DUMMY)
                {
                    if (!TestCollision(TestPos))
                    {
                        Position = TestPos;
                        DoWarp();
                    } else
                    {
                        MaxVision.PlayBonkSound();
                        MaxVision.InputController.ResetInput(new string[] { "down", "left", "right", "up"});
                    }
                    
                } 
            }
        }

        private static void DoWarp()
        {
            foreach(Warp d in OverworldMap.Warps)
            {
                if (d.TestWarp())
                {
                    return;
                }
            }
        }

        public static void Draw()
        {

            DrawDebug(
                DrawHeight,
                DrawWidth,
                Game1.ToUse,

                spriteBatch, graphics.GraphicsDevice, Color.Lime);

            MaxVision.MapController.ProximityDetectors.DrawBoxes(spriteBatch, graphics.GraphicsDevice, Game1.ToUse);

            Color color = Color.Black;

            if(MaxVision.MapController.LookedObject != null)
            {
                if(MaxVision.MapController.LookedObject.GetType() != typeof(MapCollision))
                {
                    color = Color.Lime;
                } else
                {
                    color = Color.Red;
                }
            }

            //draw look
            DrawDebug(
                4,
                4,
                MaxVision.Player.LookVector - MaxVision.Player.Position + Game1.ToUse,

                spriteBatch, graphics.GraphicsDevice, color) ;

            DrawDebug(
                4,
                4,
                MaxVision.MapController.PlayerLayer.TriggerVector - MaxVision.Player.Position + Game1.ToUse,

                spriteBatch, graphics.GraphicsDevice, Color.Magenta);

            
        }

        private static void DrawDebug(int width, int height, Vector2 position, SpriteBatch spriteBatch, GraphicsDevice GD, Color c)
        {
            if (spriteBatch != null)
            {
                Color[] data = new Color[width * height];
                Texture2D rectTexture = new Texture2D(GD, width, height);

                for (int i = 0; i < data.Length; ++i)
                    data[i] = Color.White;

                rectTexture.SetData(data);

                position = new Vector2((float)(position.X - width * .5), (float)(position.Y - height * .5));

                spriteBatch.Draw(rectTexture, position, c);
            }

        }

        private static bool TestCollision(Vector2 position)
        {
            foreach(Collision c in CollisionList)
            {
                if (c.Border.PointInGeometry(position))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
