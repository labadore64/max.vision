﻿using LineBatch;
using max.geometry.shape2D;
using max.vision;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using System.Linq;

namespace AccessibilityDemo.demo.player
{
    public class Collision
    {
        public Polygon2D Border { get; protected set; }
        SpriteBatch sprite = Game1.spriteBatch;

        public Collision(Vector2[] points)
        {
            Border = new Polygon2D(points);
        }

        public Collision(SpriteBatch s, Polygon2D p)
        {
            sprite = s;
            Border = p;
        }

        public void Draw()
        {
            if (Border.Count > 1)
            {
                List<Vector2> points = new List<Vector2>(Border.Points.ToArray());

                Vector2 current;
                Vector2 next;

                    for (int i = 0; i < points.Count; i++)
                    {
                        current = points[i];
                        next = points[(i + 1) % points.Count];
                        SpriteBatchEx.DrawLine(sprite,
                            current + Game1.ToUse - MaxVision.SoundController.Position,
                            next + Game1.ToUse - MaxVision.SoundController.Position,
                            Color.Blue, 1);
                    }

            }
        }
    }
}
