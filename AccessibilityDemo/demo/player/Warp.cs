﻿using AccessibilityDemo.demo.map;
using AccessibilityDemo.demo.map.component;
using max.vision;
using max.geometry.shape2D;
using Microsoft.Xna.Framework;
using System;

namespace AccessibilityDemo.demo.player
{
    public class Warp
    {
        public Polygon2D Border { get; protected set; }

        public WarpDestinationData Destination;
        public Map DestinationMap;

        const int WARP_TIME = 30;
        public Warp(Vector2[] points)
        {
            Border = new Polygon2D(points);
        }

        //returns true if a warp was detected and initiated.
        public bool TestWarp()
        {
            if (Border.PointInGeometry(Player.Position))
            {
                return TriggerWarp();
            }

            return false;
        }

        private bool TriggerWarp()
        {
            if (DestinationMap == DataContainer.CurrentMap)
            {
                Player.Position = Destination.Position;
            } else
            {
                Player.Position = Destination.Position;
                DataContainer.LoadMap(DestinationMap);
            }
            MaxVision.PlaySound("sound_fx","warp");
            return true;
        }
    }
}
