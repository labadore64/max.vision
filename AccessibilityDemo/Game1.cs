﻿using AccessibilityDemo.demo.player;
using max.vision;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using LineBatch;
using AccessibilityDemo.demo;
using AccessibilityDemo.demo.map.component;
using Microsoft.Xna.Framework.Input;
using max.input.handler.keyboard;
using max.input.group;
using System;
using max.input.container.keyboard;
using max.input;
using max.input.container.types.component;
using max.sound.xna;

namespace AccessibilityDemo
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        public static Vector2 ToUse = Vector2.Zero;
        public static readonly Vector2 CenterOfScreen = new Vector2(400, 250);
        GraphicsDeviceManager graphics;
        public static SpriteBatch spriteBatch;
        public static bool FollowCharacter = false;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            MaxVision.LoadAllText();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            //initialize sound collision list
            SoundEffectData.DoList();
            SetMaxSeerOptions();
            MaxVision.Initialize(new SoundResourceManagerXNA(Content));

            MaxVision.LoadSoundEffectsFromDirectory("sound_fx");
            AddKeys();
            base.Initialize();

            }

        private void SetMaxSeerOptions()
        {
            MaxVision.SoundController.Options.EnvironmentVolume = 10f;
            MaxVision.Options.Sound.ProximityMaxDistance = 50f;
            MaxVision.Options.Sound.ProximityVolume = .5f;
            MaxVision.Options.AXLabel.AXLabelTriggerDistance = 30f;
            //MaxVision.Options.Sound.MaxSeerSoundDirectory = "";

        }

        private void AddKeys()
        {
            InputGroup Group = MaxVision.InputController.AddGroup("BasicInput");
            Group.Enable();
            KeyboardHandler KeyboardDef = Group.AddKeyboardHandler();

            RegularInput(KeyboardDef);
            //PathTestInput(KeyboardDef);

            MaxVision.InputController.EnableGroup("BasicInput");
        }

        private void PathTestInput(KeyboardHandler KeyboardDef)
        {
            KeyboardDef.CreateBasic("back", Keys.Up);
            KeyboardDef.CreateBasic("forward", Keys.Down);
        }

        private void RegularInput(KeyboardHandler KeyboardDef)
        {
            
            BasicKeyboardInputContainer key;

            key = KeyboardDef.CreateBasic("select", 'Z');
            key.Timer.SetCooldownTimer(100);

            KeyboardDef.CreateBasic("test", 'V');

            key = KeyboardDef.CreateBasic("cancel", 'X');
            key.Timer.SetCooldownTimer(300L);

            KeyboardDef.CreateBasic("coords", 'C');

            key = KeyboardDef.CreateBasic("up", Keys.Up);
            key.Timer.SetDoubleTapTimer(300L);

            key = KeyboardDef.CreateBasic("down", Keys.Down);
            key.Timer.SetDoubleTapTimer(300L);

            key = KeyboardDef.CreateBasic("left", Keys.Left);
            key.Timer.SetDoubleTapTimer(300L);

            key = KeyboardDef.CreateBasic("right", Keys.Right);
            key.Timer.SetDoubleTapTimer(300L);

            key = KeyboardDef.CreateBasic("clockwise", Keys.Q);
            key.Timer.SetDoubleTapTimer(300L);

            key = KeyboardDef.CreateBasic("counterclockwise", Keys.W);
            key.Timer.SetDoubleTapTimer(300L);

            key = KeyboardDef.CreateBasic("zoomin", Keys.A);
            key.Timer.SetDoubleTapTimer(300L);

            key = KeyboardDef.CreateBasic("zoomout", Keys.S);
            key.Timer.SetDoubleTapTimer(300L);

            for (int i = 0; i < 8; i++)
            {
                KeyboardDef.CreateBasic(InputTypes.Access[i], (i + 1).ToString());
            }

            LinkedKeyboardInputContainer linked;

            //testing Linked
            linked = (LinkedKeyboardInputContainer)KeyboardDef.CreateLinked("linkedpress");
            LinkedInput link = linked.InputAdd("A");
            linked.InputStartFrame(10);
            linked.InputEndFrame(1000);

            linked.InputAdd("S");
            linked.InputStartFrame(10);
            linked.InputEndFrame(10000);

            linked.InputAdd("D");
            linked.InputStartFrame(10);
            linked.InputEndFrame(1000);

            linked.InputAdd("F");
            linked.InputStartFrame(10);
            linked.InputEndFrame(10000);
            linked.InputSetNext(link);
            linked.Timer.EndFrame = 200;
            linked.Timer.UseStopwatch = false;

            //Dictionary<string,object>  test = MaxSeer.InputController.Serialize();
            MaxVision.InputController.SaveAll();
            
            //MaxVision.InputController.LoadGroup("BasicInput");
            //MaxVision.InputController.EnableGroup("BasicInput");
            
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            SpriteBatchEx.GraphicsDevice = GraphicsDevice;
            //load player stuff
            DemoStuff();

            //TestPath();
        }

        private void DemoStuff()
        {
            Player.Initialize(graphics, spriteBatch);
            // TODO: use this.Content to load your game content here

            DataContainer.LoadMapsFromFile("LevelData.xml");
            DataContainer.LoadMap("Foyer");

            MaxVision.MapController.WriteLayers();

            //MaxVision.MapController.ReadLayers();

            MaxVision.Options.Sound.ProximityEnabled = false;
            MaxVision.SoundController.LookAngle = (float)(Math.PI * .5);

            Player.Position = new Vector2(200, 200);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Saves the bookmarks of the current room before closing
        /// </summary>
        protected override void EndRun()
        {
            MaxVision.SaveBookmarks(OverworldMap.Map.Name);
            base.EndRun();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values</param>
        protected override void Update(GameTime gameTime)
        {
            //update input
            MaxVision.Update();
            DemoUpdate();
            //DemoPathUpdate();
            //update input data.
            base.Update(gameTime);
        }

        private void DemoUpdate()
        {

            //update player
            Player.Update();
            //update menu

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values</param>
        protected override void Draw(GameTime gameTime)
        {
            ToUse = Game1.CenterOfScreen;

            if (!FollowCharacter)
            {
                ToUse = MaxVision.PlayerPostion;
            }

            DemoDraw();
            //DemoPathDraw();
            base.Draw(gameTime);
        }

        private void DemoDraw()
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();
            Player.Draw();
            foreach (Collision c in OverworldMap.Collisions)
            {
                c.Draw();
            }

            spriteBatch.End();

        }

        private void DemoPathDraw()
        {
            spriteBatch.Begin();
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here

            DrawDebug(MaxVision.PlayerPostion, 3, Color.Lime);
            spriteBatch.End();
        }

        private void DrawDebug(Vector2 Position, int size, Color c)
        {
            DrawDebug(
                size,
                size,
                (int)(Position.X),
                (int)(Position.Y), c);
        }

        private void DrawDebug(int width, int height, int left, int top, Color c)
        {
            if (spriteBatch != null)
            {
                Color[] data = new Color[width * height];
                Texture2D rectTexture = new Texture2D(GraphicsDevice, width, height);

                for (int i = 0; i < data.Length; ++i)
                    data[i] = Color.White;

                rectTexture.SetData(data);
                var position = new Vector2(left - width * .5f, top - height * .5f);

                spriteBatch.Draw(rectTexture, position, c);
            }

        }
    }
}
