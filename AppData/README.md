These are the default config files for MaxLib.

Place them in ``%appdata%`` in a directory called MaxLib. 

MaxLib games can use a similar config directory for their configurations as well; MaxLib will be the defaults if none are found in that directory.