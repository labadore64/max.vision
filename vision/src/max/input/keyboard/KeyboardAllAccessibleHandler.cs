﻿using max.input.handler.keyboard;
using max.vision;
using System;

namespace max.input.keyboard
{
    public class KeyboardAllAccessibleHandler : KeyboardAllHandler
    {
        public override void TTSRead(String text)
        {
            MaxVision.Speak(text);
        }
    }
}