﻿using System;

namespace max.tts.reader
{
    static class Reader
    {

        public static void Say(String text, int screenreader)
        {
            if (screenreader == 0)
            {
                SAPIReader.Say(text);
            } else
            {
                NVDAReader.Speak(text);
            }
        }

        public static void Say(String text)
        {
            SAPIReader.Say(text);
        }

        public static void Stop()
        {
            SAPIReader.Stop();
            NVDAReader.StopSpeaking();
        }
    }
}
