﻿using System;
using System.Runtime.InteropServices;

namespace max.tts.reader
{
    static class NVDAReader
    {
        /// <summary>Location of the NVDA64 library.</summary>
        public const String DLL_LOCATION_NVDA64 = "Lib/nvdaControllerClient64.dll";
        /// <summary>Location of the NVDA32 library.</summary>
        public const String DLL_LOCATION_NVDA32 = "Lib/nvdaControllerClient32.dll";

        /// <summary>Whether or not the NVDA reader is available.</summary>
        public static bool Available { get; private set; }
        /// <summary>The text to be read this cycle.</summary>
        static String TextRead { get; set; }
        /// <summary>Initialize the NVDA interface.</summary>
        public static void Init()
        {
            if (Environment.Is64BitProcess)
            {
                Available = NativeMethods64.nvdaController_testIfRunning() == 0;
            }
            else
            {
                Available = NativeMethods32.nvdaController_testIfRunning() == 0;
            }
        }
        /// <summary>Speak with the NVDA interface. Will interrupt text.</summary>
        /// <param name="text">The text to speak</param>
        public static void Speak(string text)
        {
            Speak(text, true);
        }
        /// <summary>Speak with the NVDA interface. Has option to interrupt text.</summary>
        /// <param name="text">The text to speak</param>
        /// <param name="interrupt">Whether or not the text should be interrupted</param>
        public static void Speak(string text, bool interrupt)
        {
            if (Available)
            {
                if (interrupt)
                {
                    StopSpeaking();
                }
                TextRead = text;
                if (Environment.Is64BitProcess)
                {
                    NVDARead64();
                }
                else
                {
                    NVDARead32();
                }
            }

        }
        /// <summary>Initialize the 64bit NVDA reader.</summary>
        public static void NVDARead64()
        {
            NativeMethods64.nvdaController_speakText(TextRead);
        }
        /// <summary>Initialize the 32bit NVDA reader.</summary>
        public static void NVDARead32()
        {
            NativeMethods32.nvdaController_speakText(TextRead);
        }
        /// <summary>Stop the NVDA speaker from speaking.</summary>
        public static void StopSpeaking()
        {
            if (Environment.Is64BitProcess)
            {
                NativeMethods64.nvdaController_cancelSpeech();
            }
            else
            {
                NativeMethods32.nvdaController_cancelSpeech();
            }
        }
        /// <summary>NVDA 32 bit dll imports.</summary>
        internal static class NativeMethods32
        {
            [DllImport(DLL_LOCATION_NVDA32)]
            internal static extern int nvdaController_testIfRunning();

            [DllImport(DLL_LOCATION_NVDA32, CharSet = CharSet.Auto)]
            internal static extern int nvdaController_speakText(string text);

            [DllImport(DLL_LOCATION_NVDA32)]
            internal static extern int nvdaController_cancelSpeech();
        }
        /// <summary>NVDA 64 bit dll imports.</summary>
        internal static class NativeMethods64
        {
            [DllImport(DLL_LOCATION_NVDA64)]
            internal static extern int nvdaController_testIfRunning();

            [DllImport(DLL_LOCATION_NVDA64, CharSet = CharSet.Auto)]
            internal static extern int nvdaController_speakText(string text);

            [DllImport(DLL_LOCATION_NVDA64)]
            internal static extern int nvdaController_cancelSpeech();
        }
    }
}