﻿
using System.Speech.Synthesis;

namespace max.tts.reader
{
    static class SAPIReader
    {
        /// <summary>SAPI Speech synthesizer.</summary>
        private readonly static SpeechSynthesizer s = new SpeechSynthesizer();

        /// <summary>Whether or not the SAPI reader is available. Always returns true.</summary>
        public static bool Available()
        {
            return true;
        }
        /// <summary>Speak with SAPI synthesizer.</summary>
        /// <param name="text">Text to be read</param>
        public static void Say(string text)
        {
            
            s.SpeakAsync(text);
        }
        /// <summary>Stop the SAPI synthesizer from speaking.
        /// </summary>
        public static void Stop()
        {
            s.SpeakAsyncCancelAll();
        }
    }
}
