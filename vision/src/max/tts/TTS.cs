﻿using max.vision;
using max.tts.reader;
using max.util;
using Microsoft.Xna.Framework.Input;
using System;

namespace max.tts
{
    static class TTS
    {
        public const int TTS_READ_MAXLENGTH = 2000;
        /// <summary>
        /// <para>The current text loaded for the screen reader to read.</para>
        /// </summary>
        static string Text { get; set; } = "";
        /// <summary>
        /// <para>The int ID of the screen reader currently being used.</para>
        /// </summary>
        static int SCREEN_READER_ID { get; set; }
        /// <summary>
        /// <para>Represents SAPI screen reader (default).</para>
        /// </summary>
        const int SCREEN_READER_SAPI = 0;
        /// <summary>
        /// <para>Represents NVDA screen reader.</para>
        /// </summary>
        const int SCREEN_READER_NVDA = 1;

        //these are keyboard states that combined will represent which keys are pressed at the same time
        static KeyboardState CurrentState = Keyboard.GetState();
        static KeyboardState PreviousState = CurrentState;

        /// <summary>
        /// <para>Initializes the TTS engine.</para>
        /// </summary>
        public static void Initialize()
        {
            NVDAReader.Init();
            if (PlatformUtil.Platform != PlatformUtil.PlatformType.WINDOWS) { 
                Console.WriteLine("Note! TTS not currently supported on UNIX systems!");
            } else
            {
                //if windows, initialize which screen reader to use.
                if (NVDAReader.Available)
                {
                    SCREEN_READER_ID = SCREEN_READER_NVDA;
                } else
                {
                    SCREEN_READER_ID = SCREEN_READER_SAPI;
                }
            }
        }

        /// <summary>
        /// <para>Clears the text to be read.</para>
        /// </summary>
        public static void Clear()
        {
            Text = "";
        }

        /// <summary>
        /// <para>Adds text to the text speaking buffer.</para>
        /// </summary>
        public static void Say(string text)
        {
            Text += " " + text;
            Text = StringUtil.ClipString(Text, TTS_READ_MAXLENGTH);
        }

        /// <summary>
        /// <para>Updates the TTS engine. Should be called every cycle.</para>
        /// </summary>
        public static void Update()
        {
            PreviousState = CurrentState;
            CurrentState = Keyboard.GetState();
            if (MaxVision.Options.Sound.TTSEnabled)
            {
                if (!Text.Equals(""))
                {
                    if (PlatformUtil.Platform == PlatformUtil.PlatformType.WINDOWS)
                    {
                        if (MaxVision.Options.Active)
                        {
                            //This is a bug workaround for NVDA.
                            //When you're pressing a key down, 
                            //NVDA doesn't like that.
                            //Use SAPI instead.
                            if (TestNoKeysPressed())
                            {
                                Reader.Stop();
                                Reader.Say(Text, SCREEN_READER_ID);
                            }
                            else
                            {
                                SAPIReader.Stop();
                                SAPIReader.Say(Text);
                            }
                        }
                    }
                    Console.WriteLine("TTS: " + Text);
                }
            }
            Text = "";
        }

        /// <summary>
        /// <para>Stops the current text being read.</para>
        /// </summary>
        public static void Stop()
        {
            if (MaxVision.Options.Sound.TTSEnabled)
            {
                if (PlatformUtil.Platform == PlatformUtil.PlatformType.WINDOWS)
                {
                    Reader.Stop();
                }
            }
        }
        /// <summary>
        /// <para>Tests to see if any keys are being pressed.</para>
        /// </summary>
        private static bool TestNoKeysPressed()
        {
            Keys[] CurrentKeys = CurrentState.GetPressedKeys();
            Keys[] PreviousKeys = PreviousState.GetPressedKeys();

            for(int i = 0; i < CurrentKeys.Length; i++) { 
                for(int j = 0; j < PreviousKeys.Length; j++)
                {
                    if (CurrentKeys[i] == PreviousKeys[j])
                    {
                        return false;
                    }
                }
            }

            return true;
        }
    } 
}
