﻿using max.options.optiongroup;

namespace max.option
{
    public sealed class MaxSeerOptions
    {
        public bool Active { get; set; } = true;
        public AXLabelOptions AXLabel = new AXLabelOptions();
        public MapOptions Map = new MapOptions();
        public SoundOptions Sound = new SoundOptions();
    }
}
