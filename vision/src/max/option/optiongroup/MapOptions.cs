﻿using max.geometry.pathfinder;

namespace max.options.optiongroup
{
    /// <summary>
    /// This class contains all the map based options for max.maxseer.
    /// </summary>
    public sealed class MapOptions
    {
        /// <summary>
        /// <para>The maximum number of nodes that the pathfinding engine will use at any time.</para>
        /// </summary>
        /// <value>Quantity</value>
        public int MaxNodes {
            get
            {
                return MapPathFinder.Options.MaxNodes;
            }
            set
            {
                MapPathFinder.Options.MaxNodes = value;
            }
        }
        /// <summary>
        /// <para>The distance between nodes for pathfinding.</para>
        /// <para>The higher this value, the less nodes are created and less accurate the pathfinding.</para>
        /// </summary>
        /// <value>Distance</value>
        public int NodeSpacing {
            get { return MapPathFinder.Options.NodeSpacing; }
            set { MapPathFinder.Options.NodeSpacing = value; }
        }

        /// <summary>
        /// <para>The map width in pixels.</para>
        /// </summary>
        /// <value>Pixels</value>
        public int Width {
            get { return MapPathFinder.Options.Width; }
            set { MapPathFinder.Options.Width = value; }
        } 
        /// <summary>
        /// <para>The map height in pixels.</para>
        /// </summary>
        /// <value>Pixels</value>
        public int Height {
            get { return MapPathFinder.Options.Height; }
            set { MapPathFinder.Options.Height = value; }
        }

        /// <summary>
        /// Whether or not the sight mechanic is enabled for this map.
        /// </summary>
        public bool SightEnabled
        {
            get; set;
        } = true;
    }
}