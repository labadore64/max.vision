﻿
namespace max.options.optiongroup
{
    /// <summary>
    /// This class contains all the label based options for max.maxseer.
    /// </summary>
    public sealed class AXLabelOptions
    {
        /// <summary>
        /// <para>The default distance an AX label is triggered from.</para>
        /// </summary>
        /// <value>Distance</value>
        public float AXLabelTriggerDistance { get; set; } = 30f;

        /// <summary>
        /// <para>The default distance a bookmark is triggered from.</para>
        /// </summary>
        /// <value>Distance</value>
        public float BookmarkTriggerDistance { get; set; } = 30f;
    }
}
