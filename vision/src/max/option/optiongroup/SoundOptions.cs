﻿using max.option;
using max.sound.effect;
using max.vision;
using System;

namespace max.options.optiongroup
{
    /// <summary>
    /// This class contains all the sound based options for max.maxseer.
    /// </summary>
    public sealed class SoundOptions
    {
        public float ProximityVolume { get; set; } = 1;

        /// <summary>
        /// <para>The resource for the pathfinding sound. Must be 3D sound.</para>
        /// </summary>
        /// <value>Path</value>
        internal MaxSoundInstance SoundPathSoundEffect { get; set; }
        /// <summary>
        /// <para>The resource name for the pathfinding sound. Must be 3D sound.</para>
        /// </summary>
        /// <value>Path</value>
        public string SoundPathSoundFilename { get; set; } = "maxseer\\path";
        /// <summary>
        /// <para>The resource for the wall proximity sound.</para>
        /// </summary>
        /// <value>Path</value>
        internal MaxSoundInstance ProximitySoundEffect { get; set; }
        /// <summary>
        /// <para>The resource name for the wall proximity sound.</para>
        /// </summary>
        /// <value>Path</value>
        public string ProximityFilename { get; set; } = "maxseer\\wall";
        /// <summary>
        /// <para>The speed that sound paths travel at, by default.</para>
        /// </summary>
        /// <value>Speed</value>
        public float SoundPathSpeed { get; set; } = 2;
        /// <summary>
        /// <para>The distance that sound emitters should be from each other when generated in a sound path sequence.</para>
        /// </summary>
        /// <value>Distance</value>
        public float SoundPathDistance { get; set; } = 50f;
        /// <summary>
        /// <para>Cutoff distance factor. The larger this value the smaller the cutoff distance.</para>
        /// </summary>
        /// <value>Cutoff</value>
        public float EmitterCutoff { get; set; } = .3f;
        /// <summary>
        /// <para>The maximum distance the proximity detector detects.</para>
        /// </summary>
        /// <value>Distance</value>
        public float ProximityMaxDistance { get; set; } = 25f;
        /// <summary>
        /// <para>The amount of pitch difference between up and down collisions detected by the proximity detectors.</para>
        /// <para>Should be a value from 0-1.</para>
        /// </summary>
        /// <value>Multiplier 0-1</value>
        public float ProximityPitchMultiplier { get; set; } = .5f;
        /// <summary>
        /// <para>How many proximity detectors surround the player.</para>
        /// <para>Should be a value that satisfies 2^n where n > 2. </para>
        /// </summary>
        /// <value>Quantity, where 2^n and n > 2</value>
        public int ProximityCount { get; set; } = 8;

        /// <summary>
        /// The percentage of change that needs to be detected on a proximity detector
        /// to alert the user to a hallway. Ranges from 0-1, the higher the value, the more
        /// precise this detection is.
        /// </summary>
        /// <value>Cutoff value, from 0-1</value>
        public float ProximityHallwayDetectionCutoff { get; set; } = .65f;

        /// <summary>
        /// Whether or not the hallway detection sound is enabled.
        /// </summary>
        /// <value>Enabled/disabled</value>
        public bool ProximityHallwayDetectionEnabled { get; set; } = true;
        /// <summary>
        /// Whether or not the proximity detector is enabled.
        /// </summary>
        /// <value>Enabled/Disabled</value>
        public bool ProximityEnabled { get; set; } = true;
        /// <summary>
        /// The resource for the footstep sound.
        /// </summary>
        /// <value>Sound Effect</value>
        public MaxSoundInstance FootstepSoundEffect { get; set; }
        /// <summary>
        /// The resource name for the default footstep sound.
        /// </summary>
        /// <value>Path</value>
        public string FootstepFilename { get; set; } = "maxseer\\footstep";
        /// <summary>
        /// How far the player should travel before a footstep sound is made.
        /// </summary>
        /// <value>Distance</value>
        public float FootstepStepDistance { get; set; } = 35;
        /// <summary>
        /// How much the footstep pitch changes with each step.
        /// </summary>
        /// <value>Distance</value>
        public float FootstepPitchChange { get; set; } = .85f;
        /// <summary>
        /// Whether or not the footstep pitch change is on or off.
        /// </summary>
        /// <value>Enabled/Disabled</value>
        public bool FootstepPitchChangeActive { get; set; } = true;
        /// <summary>
        /// Whether the footstep controller is enabled.
        /// </summary>
        /// <value>Enabled/Disabled</value>
        public bool FootstepEnabled { get; set; } = true;
        /// <summary>
        /// Whether or not Text to Speech is enabled.
        /// </summary>
        /// <value>Enabled/Disabled</value>
        public bool TTSEnabled { get; set; } = true;

        /// <summary>
        /// The directory in your content directory to add the maxseer sound effects.
        /// </summary>
        /// <value>Directory name</value>
        public string MaxSeerSoundDirectory { get; set; } = "maxseer";

        /// <summary>
        /// The location of the Bonk sound asset. This sound is played when you hit a collision.
        /// </summary>
        /// <value>Path</value>
        public string SoundBonkFilename = "maxseer\\bonk";

        /// <summary>
        /// The location of the menu select sound asset. Plays when selecting an option on a menu.
        /// </summary>
        /// <value>Path</value>
        public string SoundAcceptInputFilename = "maxseer\\accept_input";

        /// <summary>
        /// The location of the menu select sound asset. Plays when selecting an option on a menu.
        /// </summary>
        /// <value>Path</value>
        public string SoundAskInputFilename = "maxseer\\ask_input";

        /// <summary>
        /// The location of the hallway detection sound.
        /// </summary>
        /// <value>Path</value>
        public string SoundHallwayDetectionFilename = "maxseer\\hallway";
        /// <summary>
        /// <para>The resource name for the sound when you switch to seeing a collision.</para>
        /// </summary>
        /// <value>Path</value>
        public string SightCollisionFilename { get; set; } = "maxseer\\see_wall";

        /// <summary>
        /// Whether or not the Collision Sound is enabled.
        /// </summary>
        public bool SightCollisionSoundEnabled { get; set; }

        /// <summary>
        /// <para>The resource name for the sound when you switch to seeing a labeled object.</para>
        /// </summary>
        /// <value>Path</value>
        public string SightDefaultFilename { get; set; } = "maxseer\\see_object";

        /// <summary>
        /// the distance you can see through collisions.
        /// </summary>
        public float SightSeeThroughDistance { get; set; } = 5;

        /// <summary>
        /// If true, AccessibleText returns the text and description.
        /// If false, AccessibleText returns the same thing as text.
        /// </summary>
        public static bool AccessibleFullLabel { get { return MenuOptions.AccessibleFullLabel; } set { MenuOptions.AccessibleFullLabel = value; } }
    }


}
