﻿
using max.menu.component;
using max.tts;
using System;
using System.Collections.Generic;
using max.vision;

namespace max.menu
{
    /// <summary>
    /// <para>A class that can be inherited to create accessible menus easily.</para>
    /// </summary>
    public abstract class AccessibleMenu : Menu
    {

        /// <summary>
        /// Initializes the menu with no parent or options.
        /// </summary>
        public AccessibleMenu()
        {

        }
        /// <summary>
        /// Initializes the menu with no options and a reference to a parent.
        /// </summary>
        /// <param name="Parent">The parent menu</param>
        public AccessibleMenu(AccessibleMenu Parent)
        {
            this.Parent = Parent;
            ReadTitleText();
        }
        /// <summary>
        /// Initializes the menu with no parent but with options.
        /// </summary>
        /// <param name="Options">Menu options</param>
        public AccessibleMenu(List<MenuOption> Options)
        {
            for (int i = 0; i < Options.Count; i++) {
                MenuOptions.Add(Options[i]);
            }
            ReadTitleText();
        }

        /// <summary>
        /// Initializes the menu with no parent but with options.
        /// </summary>
        /// <param name="Options">Menu options</param>
        public AccessibleMenu(MenuOption[] Options)
        {
            for (int i = 0; i < Options.Length; i++)
            {
                MenuOptions.Add(Options[i]);
            }
            ReadTitleText();
        }

        /// <summary>
        /// Initializes the menu with no parent but with options.
        /// </summary>
        /// <param name="Options">Menu options</param>
        /// <param name="Parent">Parent menu</param>
        public AccessibleMenu(MenuOption[] Options, AccessibleMenu Parent)
        {
            for (int i = 0; i < Options.Length; i++) { 
                MenuOptions.Add(Options[i]);
            }
            ReadTitleText();
            this.Parent = Parent;
        }

    /// <summary>
    /// Initializes the menu with no parent but with options.
    /// </summary>
    /// <param name="Options">Menu options</param>
    /// <param name="Parent">Parent menu</param>
    public AccessibleMenu(List<MenuOption> Options, AccessibleMenu Parent)
    {
        for (int i = 0; i < Options.Count; i++) {
            MenuOptions.Add(Options[i]);
        }
        ReadTitleText();
        this.Parent = Parent;
    } 

        /// <summary>
        /// <para>Activates the "up" action.</para>
        /// </summary>
        protected override void ActionUp()
        {
            base.ActionUp();
            ReadOptionText();
        }
        /// <summary>
        /// <para>Activates the "down" action.</para>
        /// </summary>
        protected override void ActionDown()
        {
            base.ActionDown();
            ReadOptionText();
        }
        /// <summary>
        /// <para>Activates the "left" action.</para>
        /// </summary>
        protected override void ActionLeft()
        {
            base.ActionLeft();
            ReadOptionText();

        }
        /// <summary>
        /// <para>Activates the "right" action.</para>
        /// </summary>
        protected override void ActionRight()
        {
            base.ActionRight();
            ReadOptionText();
        }

        public virtual void ReadTitleText()
        {
            TTS.Say(Title);
        }

        public virtual void ReadTitleTextWithOptions()
        {
            TTS.Say(Title);
            ReadOptionText();
        }

        /// <summary>
        /// <para>Reads the text of the currently selected option..</para>
        /// </summary>
        public virtual void ReadOptionText()
        {
            if (MenuOptions.Count > 0)
            {
                if (MenuOptions[SelectedIndex] != null)
                {
                    TTS.Say(MenuOptions[SelectedIndex].AccessibleText);
                }
            }
        }

        public void SetHUDText(Dictionary<String,String> HUDData)
        {
            MaxVision.MaxHud.SetEntries(HUDData);
        }

    }
}