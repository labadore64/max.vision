﻿using max.vision;
using max.vision.stage.component.label;
using System.Collections.Generic;

namespace max.menu.nav
{
    class MaxBookmarkMenu : MaxNavigationMenu
    {

        public MaxBookmarkMenu()
        {

        }

        public override void Initialize(MenuController Controller)
        {
            base.Initialize(Controller);
            MenuOptions.Clear();
            List<AXLabel> labels = new List<AXLabel>(MaxVision.GetBookmarks());

            labels = labels.FindAll(x => x.MenuAllowed);

            LabelList = labels;


            for (int i = 0; i < LabelList.Count; i++)
            {
                AddMenuOption(LabelList[i].Name, OpenSubmenu);
            }

        }
    }
}
