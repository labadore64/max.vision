﻿using max.vision;
using max.vision.stage.component.label;
using max.menu.input;
using System;
using System.Collections.Generic;
using max.input.handler.keyboard;
using max.lang;
using max.input;

namespace max.menu.nav
{
    public class MaxNavigationSubMenu : AccessibleMenu
    {

        const string TEXT_RENAME = "option_rename";
        const string TEXT_REDESCRIBE = "option_redescribe";
        const string TEXT_PATHFIND = "option_pathfind";
        const string TEXT_CANCEL = "option_cancel";
        const string TEXT_UPDATED_NAME = "option_updatename";
        const string TEXT_UPDATED_DESC = "option_updatedesc";

        InputTextMenu TextInput;
        int Property;

        const int PROPERTY_RENAME = 1;
        const int PROPERTY_REDESCRIBE = 2;

        int delay = -1;

        const int DELAY_OPEN = 15;

        public AXLabel SelectedLabel { get; set; }
        public MaxNavigationSubMenu()
        {

        }

        public MaxNavigationSubMenu(AccessibleMenu Parent, AXLabel Label)
        {
            this.Parent = Parent;
            SelectedLabel = Label;
        
        }

        public override void Initialize(MenuController Controller)
        {

            base.Initialize(Controller);
            GenerateOptions();
            Title = SelectedLabel.Name + " " + MenuOptions[0].Text;
            //makes it so the select sound isnt played when you create a path
            IgnoreSelectSoundArray = new Action[] { this.CreatePath };
            UpdateHUDValues();
            ReadTitleText();
        }

        public override void Update()
        {
            if(delay == 0)
            {
                OpenTextInputMenu();
            }
            if(delay > -1)
            {
                delay--;
            }

            if (TextInput == null)
            {
                base.Update();
            } else
            {
                TextInput.Update();
            }
        }

        protected override void ActionLeft()
        {
            base.ActionLeft();
            UpdateHUDValues();
        }

        protected override void ActionRight()
        {
            base.ActionRight();
            UpdateHUDValues();
        }

        protected override void ActionUp()
        {
            base.ActionUp();
            UpdateHUDValues();
        }

        protected override void ActionDown()
        {
            base.ActionDown();
            UpdateHUDValues();
        }

        protected override void GenerateOptions()
        {
            Actions.Add(TEXT_RENAME,Rename);
            Actions.Add(TEXT_REDESCRIBE, ReDescribe);
            Actions.Add(TEXT_PATHFIND, CreatePath);
            Actions.Add(TEXT_CANCEL, Cancel);
            base.GenerateOptions();
        }

        private void Rename()
        {
            Property = PROPERTY_RENAME;
            delay = DELAY_OPEN;
        }

        private void ReDescribe()
        {
            Property = PROPERTY_REDESCRIBE;
            delay = DELAY_OPEN;
        }

        private void OpenTextInputMenu()
        {
            TextInput = new InputTextMenu(this);

            KeyboardHandler.StdinEnterAction = SetText;
            MenuController.Push(TextInput);
        }

        public void SetText()
        {
            MenuController.Pop();
            if (Property == PROPERTY_RENAME)
            {
                SelectedLabel.Name = TextInput.Text;
                MaxVision.Speak(LanguageManager.Get(GetType().Name,TEXT_UPDATED_NAME));
            } else if (Property == PROPERTY_REDESCRIBE)
            {
                SelectedLabel.Description = TextInput.Text;
                MaxVision.Speak(LanguageManager.Get(GetType().Name, TEXT_UPDATED_DESC));
            }
            SelectedIndex = 0;
            MaxVision.Speak(MenuOptions[0].Text);
            TextInput = null;
            Property = 0;
            UpdateHUDValues();
            if (Parent != null)
            {
                Parent.UpdateMenuList();
            }
        }

        private void CreatePath()
        {
            if (SelectedLabel != null)
            {
                MaxVision.GenerateLocatorPath(SelectedLabel);
                Cancel();
                Parent.Cancel();
            }
        }

        private void UpdateHUDValues()
        {
            Dictionary<string, string> HUDValues = new Dictionary<string, string>();

            HUDValues.Add(InputTypes.Access[0], SelectedLabel.Name);
            HUDValues.Add(InputTypes.Access[1], SelectedLabel.Description);
            HUDValues.Add(InputTypes.Access[2],
                MaxVision.GetLangText( "coords_2D",

                    new string[]{
                        string.Format("{0:N0}", SelectedLabel.Position.X),
                        string.Format("{0:N0}", SelectedLabel.Position.Y)
                    }
                )
           );

            SetHUDText(HUDValues);
        }

        /// <summary>
        /// Updates the menu list. Does nothing for this class.
        /// </summary>
        public override void UpdateMenuList()
        {

        }

    }
}
