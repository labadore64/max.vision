﻿using max.input;
using max.vision;
using max.vision.stage.component.label;
using System.Collections.Generic;

namespace max.menu.nav
{
    class MaxNavigationMenu : AccessibleMenu
    {

        protected List<AXLabel> LabelList = new List<AXLabel>();
        public MaxNavigationSubMenu SubMenu { get; protected set; } = null;

        internal bool Active = false;

        public MaxNavigationMenu()
        {

        }

        public override void Initialize(MenuController Controller)
        {
            base.Initialize(Controller);

            List<AXLabel> labels = new List<AXLabel>(MaxVision.GetAXLabels());

            labels = labels.FindAll(x => x.MenuAllowed);

            LabelList = labels.FindAll(s => s.IsRegular);

            UpdateHUDValues();
            UpdateMenuList();
        }

        protected void OpenSubmenu()
        {
            SubMenu = new MaxNavigationSubMenu(this, LabelList[SelectedIndex]);
            MenuController.Push(SubMenu);
        }

        protected override void ActionLeft()
        {
            //do nothing. Right/Left is handled by the parent menu.
        }

        protected override void ActionRight()
        {
            //do nothing. Right/Left is handled by the parent menu.
        }

        protected override void ActionUp()
        {
            if (Active)
            {
                if (LabelList.Count > 0)
                {
                    base.ActionUp();
                    UpdateHUDValues();
                }
            }
        }

        protected override void ActionDown()
        {
            if (Active)
            {
                if (LabelList.Count > 0)
                {
                    base.ActionDown();
                    UpdateHUDValues();
                }
            }
        }

        internal void UpdateHUDValues()
        {
            Dictionary<string, string> HUDValues = new Dictionary<string, string>();
            List<AXLabel> labels = LabelList;
            if (labels.Count > 0)
            {
                AXLabel selectedLabel = labels[SelectedIndex];

                HUDValues.Add(InputTypes.Access[0], selectedLabel.Name);
                HUDValues.Add(InputTypes.Access[1], selectedLabel.Description);

                HUDValues.Add(InputTypes.Access[2],
                    MaxVision.GetLangText("coords_2D",
                        new string[]{
                        string.Format("{0:N0}", selectedLabel.Position.X),
                        string.Format("{0:N0}", selectedLabel.Position.Y)
                        }
                    )
               );
            }

            SetHUDText(HUDValues);
        }

        public override void Update()
        {
            if (Active)
            {
                if (SubMenu == null)
                {
                    base.Update();
                }
                else
                {
                    SubMenu.Update();
                }
            }
        }

        public override void Reactivate()
        {
            SubMenu = null;
        }

        public override void UpdateMenuList()
        {
            MenuOptions.Clear();
            for (int i = 0; i < LabelList.Count; i++)
            {
                AddMenuOption(LabelList[i].Name, OpenSubmenu);
            }
        }

    }
}
