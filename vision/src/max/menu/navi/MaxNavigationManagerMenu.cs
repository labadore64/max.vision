﻿using max.input;
using max.vision;

namespace max.menu.nav
{
    public class MaxNavigationManagerMenu : AccessibleMenu
    {
        MaxBookmarkMenu BMenu = new MaxBookmarkMenu();
        MaxNavigationMenu NavMenu = new MaxNavigationMenu();
        MaxNavigationMenu SelectedMenu;

        public MaxNavigationManagerMenu()
        {

        }

        public override void Initialize(MenuController Controller)
        {
            base.Initialize(Controller);
            SelectedMenu = NavMenu;
            ReadTitleText();
            PlayOpenSound();
            BMenu.Initialize(MenuController);
            NavMenu.Initialize(MenuController);

            if(NavMenu.Count == 0)
            {
                SelectedMenu.Active = false;
                SelectedMenu = BMenu;
                SelectedMenu.Active = true;
                SelectedMenu.UpdateHUDValues();
                SelectedMenu.ReadTitleText();
            }
            SelectedMenu.Active = true;
        }

        protected override void ActionUp()
        {
            //do nothing
        }

        protected override void ActionDown()
        {
            //do nothing
        }

        protected override void ActionLeft()
        {
            if(NavMenu.Count > 0)
            {
                SelectedMenu.Active = false;
                SelectedMenu = NavMenu;
                SelectedMenu.Active = true;
                SelectedMenu.UpdateHUDValues();
                SelectedMenu.ReadTitleText();
                PlayMoveSound();
            }
        }

        protected override void ActionRight()
        {
            if (BMenu.Count > 0)
            {
                SelectedMenu.Active = false;
                SelectedMenu = BMenu;
                SelectedMenu.Active = true;
                SelectedMenu.UpdateHUDValues();
                SelectedMenu.ReadTitleText();
                PlayMoveSound();
            }
        }

        public override void Update()
        {
            
            MaxNavigationMenu s = SelectedMenu;
            if (s.SubMenu == null)
            {
                if (InputController.TestLeftPressed())
                {
                    ActionLeft();
                }
                else if (InputController.TestRightPressed())
                {
                    ActionRight();
                }
            }

            SelectedMenu.Update();

            if (SelectedMenu.Disposed)
            {
                Disposed = true;
            }
        }

        /// <summary>
        /// Updates the menu list. Does nothing for this class.
        /// </summary>
        public override void UpdateMenuList()
        {
            
        }

        public override void Dispose()
        {
            base.Dispose();
            BMenu.Dispose();
            NavMenu.Dispose();
            MaxVision.MenuCooldown();
        }
    }
}
