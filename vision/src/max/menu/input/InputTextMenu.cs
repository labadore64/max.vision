﻿using max.vision;
using max.input.handler.keyboard;

namespace max.menu.input
{
    public class InputTextMenu : AccessibleMenu
    {
        public string Text {get; protected set; }
        public InputTextMenu(AccessibleMenu Parent)
        {
            Title = "Input Text";
            this.Parent = Parent;

            MenuController = Parent.MenuController;

            KeyboardHandler.StandardInput = true;
            KeyboardHandler.KeyboardString = "";

            MaxVision.Speak(Title);
            if (MaxVision.MenuController.Options.MenuSoundsEnabledAlways || MaxVision.Options.Active)
            {
                MaxVision.PlaySound(MaxVision.Options.Sound.SoundAskInputFilename);
            }
        }

        public override void Dispose()
        {
            Text = KeyboardHandler.KeyboardString;
            KeyboardHandler.KeyboardString = "";
            KeyboardHandler.StandardInput = false;
            MaxVision.Speak(Text);
            if (MaxVision.MenuController.Options.MenuSoundsEnabledAlways || MaxVision.Options.Active)
            {
                MaxVision.PlaySound(MaxVision.Options.Sound.SoundAcceptInputFilename);
            }
        }

        /// <summary>
        /// Updates the menu list. Does nothing for this class.
        /// </summary>
        public override void UpdateMenuList()
        {

        }
    }
}
