﻿using max.vision.hud;
using max.vision.stage;
using max.geometry.shape2D;
using max.vision.stage.component.label;
using max.vision.stage.path;
using max.sound.effect;
using max.sound.emitter;
using max.tts;
using max.input;
using max.menu;
using max.menu.nav;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using max.option;
using System.IO;
using max.input.handler.keyboard;
using max.input.keyboard;
using static max.serialize.MaxSerializer;
using max.IO;
using max.util;
using max.lang;
using max.stage;
using max.vision.stage.component;
using max.serialize;
using stage.layer.controller;
using max.sound.controller;
using max.sound.resource;

namespace max.vision
{

    /// <summary>
    /// Main maxseer engine. It serves as a single class to wrap every part
    /// of the maxseer engine together in one place, so that developers can easily
    /// use the engine without having to mess around with the controllers or objects.
    /// </summary>
    public static class MaxVision
    {

        #region consts
        /// <summary>
        /// <para>Represents the Keyboard in the array of inputs for InputController.</para>
        /// <para>Primarily used for serializing/deserializing input config files and making sure they're in the right order.</para>
        /// </summary>
        /// <value>Keyboard Input</value>
        public const int INPUT_KEYBOARD = 0;

        /// <summary>Represents the current key in a "no interaction" state.</summary>
        /// <value>No interaction</value>
        public const int INPUT_STATE_NONE = 0;
        /// <summary>Represents the current key in a "pressed" state.</summary>
        /// <value>Key is pressed</value>
        public const int INPUT_STATE_PRESSED = 1;
        /// <summary>Represents the current key in a "held" state.</summary>
        /// <value>Key is held</value>
        public const int INPUT_STATE_HOLD = 2;
        /// <summary>Represents the current key in a "released" state.</summary>
        /// <value>Key is released</value>
        public const int INPUT_STATE_RELEASED = 3;

        // how many frames for the cooldown
        const int COOLDOWN_RATE = 2;

        // serialization variables
        const string SER_BOOKMARK = "bm";
        #endregion

        #region vars

        public static StageController StageController { get; private set; } = new StageController();
        public static AXMapController MapController { get; set; }
        public static MenuController MenuController { get; private set; }
        public static SoundController SoundController = new SoundController();
        public static InputController InputController { get; private set; } = new InputController();

        public static MaxHud MaxHud { get; private set; }
        internal static AccessibleMenu Menu = null;
        public static MaxSeerOptions Options = new MaxSeerOptions();

        static int cooldown = -1;

        public static SerialFormat ConfigType = SerialFormat.YAML;

        public static string FileExt = ".dat";
        public static string ConfigDirectory = "config\\maxvision";
        public static string BookmarkDirectory = "bookmark";

        /// <summary>
        /// The directory of the default max vision lang files.
        /// </summary>
        public const string LangMaxMenuDirectory = "max\\menu";

        /// <summary>
        /// The directory of normal lang files.
        /// </summary>
        public const string LangMenuDirectory = "menu";
        public const string LangTextName = "vision";
        #endregion

        #region properties

        /// <summary>
        /// Returns if a menu is currently active in max.maxseer.
        /// For example if using any of the default navigation menus,
        /// it will return true, so you can disable inputs.
        /// </summary>
        /// <value>Whether or not menus are active</value>
        static public bool MenuActive
        {
            get
            {
                if(Menu == null)
                {
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Returns true if the menu cooldown is still active.
        /// This prevents menus opening instantly after closing
        /// them and other annoying behaviour.
        /// </summary>
        /// <value>Whether cooldown is active</value>
        static public bool MenuCoolDownActive
        {
            get
            {
                if(cooldown > -1)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        }

        public static Player2DController Player
        {
            get
            {
                return MapController.PlayerLayer.PlayerController;
            }
        }

        /// <summary>
        /// The position of the sound listener.
        /// </summary>
        /// <value>Listener position</value>
        public static Vector2 PlayerPostion
        {
            get
            {
                return SoundController.Position;
            }
            set
            {
                SoundController.Position = new Vector2(value.X, value.Y);
            }
        }

        /// <summary>
        /// The last position of the sound listener.
        /// </summary>
        /// <value>Last listener position</value>
        public static Vector2 PlayerLastPosition
        {
            get
            {
                return SoundController.LastPosition;
            }
        }

        #endregion

        #region controller
        /// <summary>
        /// <para>Initializes the MaxSeer engine. Execute this after your content manager has been initialized.</para>
        /// </summary>
        /// <param name="C">The game's content manager</param>
        public static void Initialize(ISoundResourceManager C)
        {
            MapController = new AXMapController(StageController);
            SoundController.ResourceManager = C;
            C.SoundController = SoundController;
            //if this value is 0, make sure its set to 1
            if (Options.Sound.ProximityMaxDistance == 0)
            {
                Options.Sound.ProximityMaxDistance = 1;
            }
            LoadBaseSoundEffects();
            TTS.Initialize();
            SoundController.Initialize();
            Validate();
            if (MenuController == null)
            {
                MenuController = new MenuController(SoundController,InputController);
            }
            MapController.Initialize();
            KeyboardHandler.stdin = new KeyboardAllAccessibleHandler();
            MaxHud = new MaxHud();

            MaxSerializedObject.InitializeTypes();
        }

        private static void LoadBaseSoundEffects()
        {
            //first load the sounds
            LoadSoundEffectsFromDirectory(Options.Sound.MaxSeerSoundDirectory);

            //then set the options to the sound effects
            LoadMaxSeerSoundEffects();
        }

        /// <summary>
        /// <para>Updates the engine. Call this method every game cycle.</para>
        /// </summary>
        public static void Update()
        {
            //basically a timer used for opening menus
            if(cooldown > -1)
            {
                cooldown--;
            }

            InputController.Update();
            if (Options.Active)
            {
                TTS.Update();
                MapController.Update();
                SoundController.Update();
                MenuController.Update();
                MaxHud.Update();
            } 
        }

        /// <summary>
        /// Makes maxseer use a custom SoundController instead.
        /// Please note that this is advanced functionality.
        /// You must make a class that inherits the SoundController interface,
        /// and it must be capable of managing the same sound processes.
        /// See How It Works on the wiki for more information.
        /// </summary>
        /// <param name="Controller">The custom Sound Controller to use</param>
        /// <param name="SoundListener">The custom Sound Listener to use</param>
        public static void UpdateSoundController(SoundController Controller)
        {
            SoundController = Controller;
        }


        private static void LoadMaxSeerSoundEffects()
        {
            Options.Sound.ProximitySoundEffect = CreateSoundInstance(Options.Sound.ProximityFilename);
            Options.Sound.FootstepSoundEffect = CreateSoundInstance(Options.Sound.FootstepFilename);
            Options.Sound.SoundPathSoundEffect = CreateSoundInstance(Options.Sound.SoundPathSoundFilename);
        }

        private static void Validate()
        {
            string errormsg = "";

            if (Options.Sound.ProximitySoundEffect == null)
            {
                errormsg += "Could not find file \"" + Options.Sound.ProximityFilename + "\" for proximity detector sound. ";
            }
            if (Options.Sound.FootstepSoundEffect == null)
            {
                errormsg += "Could not find file \"" + Options.Sound.FootstepFilename + "\" for footstep sound. ";
            }
            if (Options.Sound.SoundPathSoundEffect == null)
            {
                errormsg += "Could not find file \"" + Options.Sound.SoundPathSoundFilename + "\" for path sound. ";
            }

            //TODO: Have these check for the files on the system.


            if (SoundController.GetSoundEffect(Options.Sound.SoundBonkFilename) == null)
            {
                errormsg += "Could not find file \"" + Options.Sound.SoundBonkFilename + "\" for path sound. ";
            }
            if (SoundController.GetSoundEffect(Options.Sound.SoundAcceptInputFilename) == null)
            {
                errormsg += "Could not find file \"" + Options.Sound.SoundAcceptInputFilename + "\" for path sound. ";
            }
            if (SoundController.GetSoundEffect(Options.Sound.SoundAskInputFilename) == null)
            {
                errormsg += "Could not find file \"" + Options.Sound.SoundAskInputFilename + "\" for path sound. ";
            }


            if (errormsg.Length > 0)
            {
                throw new FileNotFoundException(errormsg);
            }
        }


        #endregion

        #region collision map methods
        /// <summary>
        /// <para>Clears the map of all placed objects, including collisions, 3D Sounds and Labels.</para>
        /// </summary>
        public static void MapReset()
        {
            MapController.ClearMap();
            SoundController.ClearSoundPaths();
            ClearLocatorPath();
            SoundController.ClearEmitters();
        }
        /// <summary>
        /// <para>Clears the map of all collisions.</para>
        /// </summary>
        public static void MapClearCollisions()
        {
            MapController.ClearCollisions();
        }
        /// <summary>
        /// <para>Adds a polygon to the map.</para>
        /// </summary>
        /// <param name="Polygon2D">The polygon to add</param>
        public static void MapAddCollision(MapCollision Collision)
        {
            MapController.AddCollision(Collision);
        }

        /// <summary>
        /// <para>Removes a polygon from the collision map.</para>
        /// </summary>
        /// <param name="Collision">The collision to remove</param>
        public static void MapRemoveCollision(MapCollision Collision)
        {
            MapController.RemoveCollision(Collision);
        }

        #endregion

        #region env Sound functions
        /// <summary>
        /// <para>Adds a SoundEmitter onto the map.</para>
        /// </summary>
        /// <param name="Sound">Sound asset</param>
        /// <param name="Position">Position of the 3D Sound</param>
        /// <param name="Distance">The cutoff Distance of the Sound</param>
        /// <returns>Generated SoundEmitter</returns>  
        public static SoundEmitter AddSoundEmitter(string Sound, Vector2 Position, float Distance)
        {
            SoundEmitter sound = SoundController.CreateEmitter(SoundController.GetSoundEffect(Sound));
            sound.SoundArea = new Circle2D(Position, Distance);
            return sound;
        }

        /// <summary>
        /// <para>Adds a SoundEmitter onto the map.</para>
        /// </summary>
        /// <param name="Sound">Sound Effect</param>
        /// <param name="Position">Position of the 3D Sound</param>
        /// <param name="Distance">The cutoff Distance of the Sound</param>
        /// <returns>Generated SoundEmitter</returns>  
        public static SoundEmitter AddSoundEmitter(MaxSoundInstance Sound, Vector2 Position, float Distance)
        {
            SoundEmitter sound = SoundController.CreateEmitter(Sound);
            sound.SoundArea = new Circle2D(Position, Distance);
            return sound;
        }

        /// <summary>
        /// <para>Adds a SoundEmitter onto the map.</para>
        /// </summary>
        /// <param name="Sound">Sound Effect</param>
        /// <param name="Position">Position of the 3D Sound</param>
        /// <param name="Distance">The cutoff Distance of the Sound</param>
        /// <returns>Generated SoundEmitter</returns>  
        public static SoundEmitter AddSoundEmitter(IMaxSound Sound, Vector2 Position, float Distance)
        {
            SoundEmitter sound = SoundController.CreateEmitter(Sound);
            sound.SoundArea = new Circle2D(Position, Distance);
            return sound;
        }

        /// <summary>
        /// <para>Adds a SoundEmitter onto the map.</para>
        /// </summary>
        /// <param name="Sound">Sound asset</param>
        /// <param name="X">X coordinate of the 3D Sound</param>
        /// <param name="Y">Y coordinate the 3D Sound</param>
        /// <param name="Distance">The cutoff Distance of the Sound</param>
        /// <returns>Generated SoundEmitter</returns>  
        public static SoundEmitter AddSoundEmitter(string Sound, float X, float Y, float Distance)
        {
            return AddSoundEmitter(SoundController.GetSoundEffect(Sound), new Vector2(X, Y), Distance);
        }

        /// <summary>
        /// <para>Adds a SoundEmitter onto the map.</para>
        /// </summary>
        /// <param name="Sound">Sound Effect</param>
        /// <param name="X">X coordinate of the 3D Sound</param>
        /// <param name="Y">Y coordinate the 3D Sound</param>
        /// <param name="Distance">The cutoff Distance of the Sound</param>
        /// <returns>Generated SoundEmitter</returns>  
        public static SoundEmitter AddSoundEmitter(MaxSoundInstance Sound, float X, float Y, float Distance)
        {
            return AddSoundEmitter(Sound, new Vector2(X, Y), Distance);
        }

        /// <summary>
        /// <para>Adds a SoundEmitter onto the map using the default cutoff Distance.</para>
        /// </summary>
        /// <param name="Sound">Sound asset</param>
        /// <param name="Position">Position of the 3D Sound</param>
        /// <returns>Generated SoundEmitter</returns>  
        public static SoundEmitter AddSoundEmitter(string Sound, Vector2 Position)
        {
            return AddSoundEmitter(SoundController.GetSoundEffect(Sound), Position, 25);
        }

        /// <summary>
        /// <para>Adds a SoundEmitter onto the map using the default cutoff Distance.</para>
        /// </summary>
        /// <param name="Sound">Sound Effect</param>
        /// <param name="Position">Position of the 3D Sound</param>
        /// <returns>Generated SoundEmitter</returns>  
        public static SoundEmitter AddSoundEmitter(MaxSoundInstance Sound, Vector2 Position)
        {
            return AddSoundEmitter(Sound, Position, 25);
        }

        /// <summary>
        /// <para>Removes a Sound emitter from the map.</para>
        /// </summary>
        /// <param name="ID">Sound emitter</param>
        /// <returns>Generated SoundEmitter</returns>  
        public static void RemoveSoundEmitter(SoundEmitter ID)
        {
            SoundController.RemoveEmitter(ID);
        }
        /// <summary>
        /// <para>Clears all Sound emitters.</para>
        /// </summary>
        public static void ClearSoundEmitters()
        {
            SoundController.ClearEmitters();
        }

        #endregion

        #region AXLabel functions
        /// <summary>
        /// <para>Create a map AXLabel.</para>
        /// </summary>
        /// <param name="Name">The AXLabel Name</param>
        /// <param name="Description">The AXLabel Description</param>
        /// <param name="Distance">The trigger Distance of reading the AXLabel Name</param>
        /// <param name="Position">The Position of the AXLabel</param>
        /// <returns>Generated AXLabel</returns>
        public static AXLabel AddAXLabel(string Name, string Description, float Distance, Vector2 Position)
        {
            return MapController.AddLabel(new AXLabel(MapController, Name, Description, Distance, Position));
        }

        /// <summary>
        /// <para>Create a map AXLabel with no Description.</para>
        /// </summary>
        /// <param name="Name">The AXLabel Name</param>
        /// <param name="Distance">The trigger Distance of reading the AXLabel Name</param>
        /// <param name="Position">The Position of the AXLabel</param>
        /// <returns>Generated AXLabel</returns>
        public static AXLabel AddAXLabel(string Name, float Distance, Vector2 Position)
        {
            return AddAXLabel(new AXLabel(MapController, Name, "", Distance, Position));
        }
        /// <summary>
        /// Adds an AXLabel to the map AXLabel list.
        /// </summary>
        /// <param name="AXLabel">The label to add to the list</param>
        /// <returns>AXLabel added</returns>
        public static AXLabel AddAXLabel(AXLabel AXLabel)
        {
            return MapController.AddLabel(AXLabel);
        }

        /// <summary>
        /// <para>Create a map AXLabel.</para>
        /// </summary>
        /// <param name="Name">The AXLabel Name</param>
        /// <param name="Description">The AXLabel Description</param>
        /// <param name="Distance">The trigger Distance of reading the AXLabel Name</param>
        /// <param name="X">The X coordinate of the AXLabel</param>
        /// <param name="Y">The Y coordinate of the AXLabel</param>
        /// <returns>Generated AXLabel</returns>
        public static AXLabel AddAXLabel(string Name, string Description, float Distance, float X, float Y)
        {
            return AddAXLabel(Name, Description, Distance, new Vector2(X, Y));
        }

        /// <summary>
        /// <para>Create a map AXLabel.</para>
        /// </summary>
        /// <param name="Name">The AXLabel Name</param>
        /// <param name="Description">The AXLabel Description</param>
        /// <param name="Polygon2D">The shape of the trigger area</param>
        /// <returns>Generated AXLabel</returns>
        public static AXLabel AddAXLabel(string Name, string Description, Polygon2D Polygon2D)
        {
            return MapController.AddLabel(new AXLabel(MapController,Name, Description, Polygon2D));
        }

        /// <summary>
        /// <para>Create a map AXLabel.</para>
        /// </summary>
        /// <param name="Name">The AXLabel Name</param>
        /// <param name="Polygon2D">The shape of the trigger area</param>
        /// <returns>Generated AXLabel</returns>
        public static AXLabel AddAXLabel(string Name, Polygon2D Polygon2D)
        {
            return AddAXLabel(Name, "", Polygon2D);
        }

        /// <summary>
        /// <para>Create a map AXLabel.</para>
        /// </summary>
        /// <param name="Name">The AXLabel Name</param>
        /// <param name="Description">The AXLabel Description</param>
        /// <param name="Polygon2D">The shape of the trigger area</param>
        /// <returns>Generated AXLabel</returns>
        public static AXLabel AddAXLabel(string Name, string Description, Vector2[] Polygon2D)
        {
            return MapController.AddLabel(new AXLabel(MapController, Name, Description, new Polygon2D(Polygon2D)));
        }

        /// <summary>
        /// <para>Clears all Labels loaded.</para>
        /// </summary>
        public static void ClearAXLabels()
        {
            MapController.ClearLabels();
        }
        /// <summary>
        /// <para>Returns the AXLabel by Name. Can return your own IAXLabels, or generated ones.</para>
        /// </summary>
        /// <param name="Name">The AXLabel Name</param>
        /// <returns>AXLabel found or null</returns>
        public static AXLabel GetAXLabel(string Name)
        {
            return MapController.GetLabelFromName(Name);
        }
        /// <summary>
        /// <para>Returns the array of Labels loaded.</para>
        /// </summary>
        /// <returns>AXLabel array</returns>
        public static List<AXLabel> GetAXLabels()
        {
            return MapController.GetLabels();
        }
        /// <summary>
        /// <para>Removes the AXLabel by Name.</para>
        /// </summary>
        /// <param name="Name">The AXLabel Name</param>
        public static void RemoveAXLabel(string Name)
        {
            MapController.RemoveLabel(GetAXLabel(Name));
        }

        /// <summary>
        /// <para>Removes the AXLabel by Name.</para>
        /// </summary>
        /// <param name="Name">The AXLabel</param>
        public static void RemoveAXLabel(AXLabel Name)
        {
            MapController.RemoveLabel(Name);
        }

        public static BookmarkLabel AddBookmark()
        {
            return AddBookmark(new BookmarkLabel());
        }

        public static BookmarkLabel AddBookmark(string Name)
        {
            BookmarkLabel l = new BookmarkLabel();
            l.Name = Name;
            return AddBookmark(l);
        }

        public static BookmarkLabel AddBookmark(string Name, string Description)
        {
            BookmarkLabel l = new BookmarkLabel();
            l.Name = Name;
            l.Description = Description;
            return AddBookmark(l);
        }

        public static BookmarkLabel AddBookmark(BookmarkLabel Label)
        {
            if (!MenuCoolDownActive)
            {
                BookmarkLabel l = BookmarkInTriggerRange(PlayerPostion);
                if (l == null)
                {
                    if (Label != null)
                    {
                        Label.Shape = new Circle2D(PlayerPostion, MaxVision.Options.AXLabel.BookmarkTriggerDistance);
                        MapController.AddBookmark(Label);
                        return Label;
                    }
                }
            }
            return null;
        }

        public static MaxSerializedObject SerializeBookmarks()
        {
            MaxSerializedObject Data = MaxSerializedObject.Create();
            List<BookmarkLabel> bookmarks = MapController.GetBookmarks();
            MaxSerializedObject[] objs = new MaxSerializedObject[bookmarks.Count];
            for (int i =0; i < bookmarks.Count; i++)
            {
                objs[i] = bookmarks[i].Serialize();
            }
            Data.AddSerializedObjectCollection(SER_BOOKMARK, objs);
            return Data;
        }

        public static void DeserializeBookmarks(MaxSerializedObject Data)
        {
            List<MaxSerializedObject> obj = Data.GetSerializedObjectCollection(SER_BOOKMARK);
            for(int i = 0; i < obj.Count; i++)
            {
                MapController.AddBookmark(new BookmarkLabel(obj[i]));
            }
        }

        public static void SaveBookmarks(string Filename)
        {
            MaxSerializedObject Data = SerializeBookmarks();
            string Filetext = "";
            if (ConfigType == SerialFormat.JSON)
            {
                Filetext = SerializeJSON(Data);
            } else if (ConfigType == SerialFormat.YAML)
            {
                Filetext = SerializeYAML(Data);
            } else if (ConfigType == SerialFormat.XML)
            {
                Filetext = SerializeXML(Data);
            }

            TextFileWriter.WriteFile("\\" + ConfigDirectory + "\\" + BookmarkDirectory, Filename,FileExt, Filetext);
        }

        public static void LoadBookmarks(string Filename)
        {
            ClearBookmarks();
            string filetext = TextFileReader.ReadFile("\\" + ConfigDirectory + "\\" + BookmarkDirectory, Filename, FileExt);

            MaxSerializedObject Data = null;

            if (ConfigType == SerialFormat.JSON)
            {
                Data = DeserializeJSON(filetext);
            }
            else if (ConfigType == SerialFormat.YAML)
            {
                Data = DeserializeYAML(filetext);
            }
            else if (ConfigType == SerialFormat.XML)
            {
                Data = DeserializeXML(filetext);
            }

            if(Data != null)
            {
                DeserializeBookmarks(Data);
            }
        }

        public static void ClearBookmarks()
        {
            MapController.ClearBookmarks();
        }

        /// <summary>
        /// <para>Removes the AXLabel by Name.</para>
        /// </summary>
        /// <param name="Name">The AXLabel Name</param>
        public static void RemoveBookmark(string Name)
        {
            MapController.RemoveBookmark(GetBookmark(Name));
        }

        /// <summary>
        /// <para>Removes the AXLabel by Name.</para>
        /// </summary>
        /// <param name="Name">The AXLabel</param>
        public static void RemoveBookmark(BookmarkLabel Name)
        {
            MapController.RemoveBookmark(Name);
        }

        public static BookmarkLabel GetBookmark(string Name)
        {
            return MapController.GetBookmarkFromName(Name);
        }

        public static List<BookmarkLabel> GetBookmarks()
        {
            return MapController.GetBookmarks();
        }

        public static BookmarkLabel BookmarkInTriggerRange(Vector2 Point)
        {
            List<BookmarkLabel> labels = MapController.GetBookmarks();

            labels = labels.FindAll(s => !s.IsRegular);


            for (int i = 0; i < labels.Count; i++)
            {
                if (labels[i].Shape.PointInGeometry(Point))
                {
                    return labels[i];
                }
            }
            return null;
        }

        /// <summary>
        /// Opens the bookmark menu for a nearby bookmark. If there is no bookmark,
        /// just opens the regular navigation menu.
        /// </summary>
        public static void OpenBookmarkMenu()
        {
            if (!MenuCoolDownActive)
            {
                BookmarkLabel l = BookmarkInTriggerRange(PlayerPostion);

                if (MenuController.MenuStack.Count == 0)
                {
                    if (l != null)
                    {
                        Menu = new MaxNavigationSubMenu(null, l);
                        if (Menu != null)
                        {
                            MenuController.Push(Menu);
                        }
                    }
                    else
                    {
                        OpenNavigationMenu();
                    }
                }
            }
        }

        /// <summary>
        /// Opens the regular navigation menu.
        /// </summary>
        public static void OpenNavigationMenu()
        {
            if (!MenuCoolDownActive)
            {
                if (GetBookmarks().Count != 0 || GetAXLabels().Count != 0)
                {
                    MenuController.Push(new MaxNavigationManagerMenu());
                }
            }
        }

        #endregion

        #region Sound path functions

        /// <summary>
        /// <para>Sets the map for the path with settings.</para> 
        /// <para>This should be initialized right after collisions have been updated.</para>
        /// </summary>
        /// <param name="NodeSpacing">The distance between nodes</param>
        /// <param name="MapWidth">The width of the map</param>
        /// <param name="MapHeight">The height of the map</param>
        public static void SetPathMap(int NodeSpacing, int MapWidth, int MapHeight)
        {
            Options.Map.NodeSpacing = NodeSpacing;
            Options.Map.Width = MapWidth;
            Options.Map.Height = MapHeight;
        }

        /// <summary>
        /// <para>Creates a path from the start Position to the end destination provided.</para>
        /// </summary>
        /// <param name="StartPosition">The start Position</param>
        /// <param name="Location">The end destination</param>
        /// <returns>Generated SoundPathSequence</returns>
        public static AXSoundPathSequence GenerateLocatorPath(AXLabel AXLabel)
        {
            if(MapController.NavigationPath != null)
            {
                SoundController.RemoveSoundPathSequence(MapController.NavigationPath.SoundPath);
            }
            MapController.GenerateLocatorPath(SoundController.Position,AXLabel);
            return MapController.NavigationPath;
        }

        /// <summary>
        /// <para>Clears the current locator path. </para>
        /// </summary>
        public static void ClearLocatorPath()
        {
            MapController.ClearLocatorPath();
        }

        #endregion

        #region Sound Effect Management

        public static void LoadSoundEffectsFromDirectory(string Directory)
        {
            SoundController.LoadSoundEffects(Directory);
        }

        public static void UnLoadSoundEffectsFromDirectory(string Directory)
        {
            SoundController.RemoveSoundBank(Directory);
        }

        public static MaxSoundInstance CreateSoundInstance(string name)
        {
            return SoundController.CreateSoundInstance(name);
        }

        public static MaxSoundInstance PlaySound(string name)
        {
            return SoundController.PlaySound(name);
        }

        public static MaxSoundInstance PlaySound(string bank, string name)
        {
            return SoundController.PlaySound(bank,name);
        }

        public static IMaxSound GetSoundEffect(string name)
        {
            return SoundController.GetSoundEffect(name);
        }

        public static IMaxSound GetSoundEffect(string bank, string name)
        {
            return SoundController.GetSoundEffect(bank, name);
        }

        #endregion

        #region Text To Speech
        /// <summary>
        /// <para>Uses the screen reader to speak.</para>
        /// </summary>
        /// <param name="Text">The text to be read</param>
        public static void Speak(string Text)
        {
            TTS.Say(Text);
        }
        /// <summary>
        /// <para>Stops the screen reader's current speech.</para>
        /// </summary>
        public static void StopSpeaking()
        {
            TTS.Stop();
        }
        /// <summary>
        /// <para>Clears any text that has been loaded this cycle for the screen reader to read. </para>
        /// </summary>
        public static void ClearSpeechText()
        {
            TTS.Clear();
        }
        #endregion

        #region Language Settings
        /// <summary>
        /// Loads all text that inherit AccessibleMenu into the language manager.
        /// </summary>
        public static void LoadMenuText()
        {
            List<Type> types = ReflectionUtil.GetTypesContainingType(typeof(AccessibleMenu));
            
            List<Type> maxTypes = types.FindAll(p => p.Assembly.GetName().Name.Contains("max."));

            for (int i = 0; i < maxTypes.Count; i++)
            {
                LanguageManager.Load(maxTypes[i].Name, LangMaxMenuDirectory, maxTypes[i].Name);
            }

            types = types.FindAll(p => !p.Assembly.GetName().Name.Contains("max."));

            for (int i = 0; i < types.Count; i++)
            {
                LanguageManager.Load(types[i].Name, LangMenuDirectory, types[i].Name);
            }
        }

        /// <summary>
        /// Loads text specific for MaxVision.
        /// </summary>
        public static void LoadText()
        {
            LanguageManager.Load(LangTextName, "max", LangTextName);
        }

        internal static string GetLangText(string ID)
        {
            return LanguageManager.Get(LangTextName, ID);
        }

        internal static string GetLangText(string ID, string[] Args)
        {
            return LanguageManager.Get(LangTextName, ID,Args);
        }

        /// <summary>
        /// Loads all parts of the text default to the max.vision engine. Includes loading accessible menu text
        /// and loading the regular max.vision text.
        /// </summary>
        public static void LoadAllText()
        {
            LoadText();
            LoadMenuText();
        }
        #endregion

        #region extras

        /// <summary>
        /// Plays the bonk sound, for when you hit a collision. If its already playing, it will not play a new sound.
        /// Make sure to add this to when you are rejected movement from collision checking so the blind player knows
        /// that they are hitting a wall.
        /// </summary>
        public static void PlayBonkSound()
        {
            if (Options.Active)
            {
                PlaySound(Options.Sound.SoundBonkFilename);
            }
        }

        /// <summary>
        /// Reads out loud the 2D coordinates of the SoundListener in a specific format.
        /// </summary>
        /// <param name="Format">Format string</param>
        public static void SpeakCoordinates(String Format)
        {
            TTS.Say(
                LanguageManager.Get(LangTextName, "coords_2D",
                    new string[]{
                        string.Format(Format, SoundController.Position.X),
                        string.Format(Format, SoundController.Position.Y)
                    }
                )
            );
        }

        /// <summary>
        /// Reads out loud the 2D coordinates of the SoundListener.
        /// </summary>
        public static void SpeakCoordinates2D()
        {
            SpeakCoordinates("{0:N0}");
        }

        /// <summary>
        /// Sets the state of the controller to menu cooldown,
        /// so that you cannot create any controller-based menus
        /// until the cooldown period passes. (5 frames)
        /// </summary>
        public static void MenuCooldown()
        {
            cooldown = COOLDOWN_RATE;
        }

        #endregion

    }
}

