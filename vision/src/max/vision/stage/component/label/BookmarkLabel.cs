﻿using max.geometry.shape2D;
using max.serialize;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace max.vision.stage.component.label
{
    public class BookmarkLabel : AXLabel
    {

        public BookmarkLabel()
        {
            Visible = false;
            IsRegular = false;
            List<BookmarkLabel> label = new List<BookmarkLabel>(MaxVision.GetBookmarks());
            Name = MaxVision.GetLangText("bookmark_default",
                new string[]{
                    (label.Count+1).ToString()
                }
            );
            Shape = new Circle2D(Vector2.Zero, MaxVision.Options.AXLabel.BookmarkTriggerDistance);
        }

        public BookmarkLabel(MaxSerializedObject Data)
        {
            Visible = false;
            IsRegular = false;
            Deserialize(Data);
        }

        public BookmarkLabel(object Data)
        {
            Deserialize((MaxSerializedObject)Data);
        }

    }
}
