﻿using max.sound.emitter;
using max.tts;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using max.util;
using max.geometry.shape2D;
using max.input;
using max.stage.component;
using max.serialize;
using max.geometry;

namespace max.vision.stage.component.label
{
    /// <summary>
    /// This class represents an accessible label that can be placed on the map.
    /// 
    /// As a listener approaches a label, the label is read out loud. Labels can have
    /// names, descriptions and sound effects attached to them.
    /// 
    /// If the SoundListener is within the distance of the TriggerDistance, it will trigger
    /// the name to be read out loud.
    /// </summary>
    public class AXLabel : GeometryComponent, ISightVisible
    {
        const string SER_NAME = "Name";
        const string SER_DESC = "Description";
        const string SER_MENU = "Menu";
        const string SER_SOUNDEFFECT = "SoundEffect";
        const string SER_VISIBLE = "Visible";
        const string SER_ISREGULAR = "IsRegular";
        const string SER_SHAPE = "shape";
        public bool SightBlock { get { return false; } }
        public virtual float ListenerDistance { get { return LineIntersectDistance(MaxVision.SoundController.LookLine); } }

        /// <summary>
        /// Sound path for when the object is "seen"
        /// </summary>
        public string SightSoundPath { get; set; } = MaxVision.Options.Sound.SightDefaultFilename;

        /// <summary>
        /// Type of label that this represents. Bookmark or Regular?
        /// </summary>
        /// <value>Whether its a bookmark or regular</value>
        public bool IsRegular { get; protected set; } = true;

        /// <summary>
        /// If the label is visible by the vision engine.
        /// </summary>
        public bool Visible { get { return _visible || IsRegular; } set { _visible = false; } }

        public Vector2 Position { get { return Shape.Center; } }

        bool _visible;

        /// <summary>
        /// Is this label added to the menu?
        /// </summary>
        /// <value>Whether it's added to the location menu</value>
        public bool MenuAllowed { get; set; } = true;

        //string data about the label, developer can place these on maps
        //and fill these in
        /// <summary>Name of the AX label.</summary>
        /// <value>Name</value>
        public string Name { get { return _name; } set { _name = StringUtil.ClipString(value, NAME_MAXLENGTH); } }
        string _name;
        const int NAME_MAXLENGTH = 50;
        /// <summary>Description of the AX label.</summary>
        /// <value>Description</value>
        public string Description { get { return _desc; } set { _desc = StringUtil.ClipString(value, DESCRIPTION_MAXLENGTH); } }
        string _desc;
        const int DESCRIPTION_MAXLENGTH = 240;

        /// <summary>Sound effect of the AX label.</summary>
        /// <value>Sound effect</value>
        public SoundEmitter SoundEffect { get; set; }

        public I2DGeometry Shape { get; set; } = new Circle2D();

        /// <summary>Position of the AX label.</summary>
        bool Triggered = false;

        protected AXMapController Controller;
        private Polygon2D polygon2D;

        public AXLabel()
        {

        }

        public AXLabel(AXMapController Controller)
        {
            Visible = true;
            Name = "";
            this.Controller = Controller;
        }

        public AXLabel(AXMapController Controller, MaxSerializedObject Data)
        {
            Visible = true;
            this.Controller = Controller;
            Deserialize(Data);
        }

        public AXLabel(AXMapController Controller,object Data)
        {
            Visible = true;
            this.Controller = Controller;
            Deserialize((MaxSerializedObject)Data);
        }

        /// <summary>
        /// Initialize a label with no name or description, but with a trigger distance and position.
        /// </summary>
        /// <param name="Distance">Trigger Distance</param>
        /// <param name="Position">Position</param>
        public AXLabel(AXMapController Controller, float Distance, Vector2 Position)
        {
            Shape = new Circle2D(Position, Distance);
            this.Controller = Controller;
        }
        /// <summary>
        /// Initialize a label with no  description, but with a name, trigger distance and position.
        /// </summary>
        /// <param name="Name">Name</param>
        /// <param name="Distance">Trigger Distance</param>
        /// <param name="Position">Position</param>
        public AXLabel(AXMapController Controller, string Name, float Distance, Vector2 Position)
        {
            Shape = new Circle2D(Position, Distance);
            this.Name = Name;
            this.Controller = Controller;
        }
        /// <summary>
        /// Initialize a label with with a name, description, trigger distance and position.
        /// </summary>
        /// <param name="Name">Name</param>
        /// <param name="Description">Description</param>
        /// <param name="Distance">Trigger Distance</param>
        /// <param name="Position">Position</param>
        public AXLabel(AXMapController Controller, string Name, string Description, float Distance, Vector2 Position)
        {
            Shape = new Circle2D(Position, Distance);
            this.Name = Name;
            this.Description = Description;
            this.Controller = Controller;
        }

        /// <summary>
        /// Creates an AX Label with a polygon shape.
        /// </summary>
        /// <param name="Controller">Controller</param>
        /// <param name="Name">Name</param>
        /// <param name="Description">Description</param>
        /// <param name="Polygon">Polygon</param>
        public AXLabel(AXMapController Controller, string Name, string Description, Polygon2D Polygon)
        {
            this.Controller = Controller;
            this.Name = Name;
            this.Description = Description;
            this.polygon2D = Polygon;
        }

        /// <summary>Updates the state of the AX label this cycle.</summary>
        public override void Update()
        {
            if (SoundEffect != null)
            {
                SoundEffect.Position = Position;
                SoundEffect.Update();
            }
            CheckTrigger();
        }
        /// <summary>Checks whether or not the label has been triggered.</summary>
        protected virtual void CheckTrigger()
        {
            Vector2 TestPosition = MaxVision.MapController.PlayerLayer.TriggerVector;

                if (Shape.PointInGeometry(TestPosition))
                {
                    TriggerEnterEffect();
                } else
                {
                    TriggerLeaveEffect();
                }

        }
        /// <summary>The effect that activates when entering the triggered area.</summary>
        protected virtual void TriggerEnterEffect()
        {
            if (!Triggered)
            {
                if (MaxVision.MapController.NavigationPath != null)
                {
                    if (MaxVision.MapController.NavigationPath.Label == this)
                    {
                        MaxVision.ClearLocatorPath();
                    }
                }
                TTS.Say(Name);
                Triggered = true;
            }
        }
        /// <summary>The effect that triggers when leaving the triggered area.</summary>
        protected virtual void TriggerLeaveEffect()
        {
            Triggered = false;
        }
        /// <summary>Disposes of the sound effect.</summary>
        public override void Dispose()
        {
            if (SoundEffect != null)
            {
                SoundEffect.Dispose();
            }
        }

        public float GetDistanceFromPoint(Vector2 Point)
        {
            return Vector2.Distance(Position, Point);
        }

        public override bool LineIntersect(Line2D Line)
        {
            return Line.IntersectPoints(Shape).Count > 0;
        }

        public override float LineIntersectDistance(Line2D Line)
        {
            return ((Circle2D)Shape).LineIntersectionDistance(Line.Points[0],Line.Points[1]);
        }

        public override float DistanceToPoint(Vector2 Point)
        {
            return Vector2.Distance(Point, Shape.Center);
        }

        public void Sight()
        {
            MaxVision.MaxHud.ClearEntries();
            //Add HUD entries here
            Dictionary<string, string> HUDValues = new Dictionary<string, string>();

            HUDValues.Add(InputTypes.Access[0], Name);
            HUDValues.Add(InputTypes.Access[1], Description);
            HUDValues.Add(InputTypes.Access[2],
            MaxVision.GetLangText( "steps_count",
                new string[]{ 
                    Math.Ceiling(Vector2.Distance(Position, MaxVision.SoundController.Position) / MaxVision.Options.Sound.FootstepStepDistance).ToString()
                }
            ));

            MaxVision.MaxHud.SetEntries(HUDValues);

            MaxVision.PlaySound(SightSoundPath);
        }

        public override int CompareTo(object obj)
        {
            if(obj == null)
            {
                return 1;
            }
            GeometryComponent ob = (GeometryComponent)obj;
            if (ob != null)
            {
                float obdist = ob.DistanceToPoint(MaxVision.SoundController.Position);
                float dist = DistanceToPoint(MaxVision.SoundController.Position);

                if (obdist < dist)
                {
                    return 1;
                }
                else if (obdist > dist)
                {
                    return -1;
                }else if(obdist == dist)
                {
                    return 0;
                }
            }

            return 1;
        }

        #region Serialize

        /// <summary>
        /// Serializes the AX Label into a Dictionary.
        /// </summary>
        /// <returns>Serialized data</returns>
        protected override MaxSerializedObject DoSerialize(MaxSerializedObject Data)
        {
            if (!StringUtil.Empty(Name))
            {
                Data.AddString(SER_NAME, Name);
            }
            if (!StringUtil.Empty(Description))
            {
                Data.AddString(SER_DESC, Description);
            }
            if (!MenuAllowed)
            {
                Data.AddBoolean(SER_MENU, MenuAllowed);
            }
            if (SoundEffect != null)
            {
                Data.AddSerializedObject(SER_SOUNDEFFECT, SoundEffect);
            }
            if (!Visible)
            {
                Data.AddBoolean(SER_VISIBLE, Visible);
            }

            Data.AddSerializedObject(SER_SHAPE,Shape);

            return Data;
        }

        /// <summary>
        /// Deserializes the dictionary into an AXLabel.
        /// </summary>
        /// <param name="Data">Data</param>
        public override void Deserialize(MaxSerializedObject Data)
        {
            Name = Data.GetString(SER_NAME);
            Description = Data.GetString(SER_DESC);
            IsRegular = Data.GetBoolean(SER_ISREGULAR);
            MenuAllowed = Data.GetBoolean(SER_MENU);
            Visible = Data.GetBoolean(SER_VISIBLE);
            
            if (SoundEffect != null)
            {
                SoundEffect.Dispose();
            }

            if (Data.ContainsKey(SER_SHAPE))
            {
                Shape = (I2DGeometry)Data.GetSerializedObject(SER_SHAPE).Deserialize();
            }

            if (Data.ContainsKey(SER_SOUNDEFFECT))
            {
                SoundEffect = new SoundEmitter(MaxVision.SoundController, Data.GetSerializedObject(SER_SOUNDEFFECT));
            }

        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new AXLabel();
            a.Deserialize(Data);
            return a;
        }

        #endregion

    }
}
