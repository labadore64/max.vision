﻿using max.geometry.shape2D;
using Microsoft.Xna.Framework;
using max.stage.component;
using max.serialize;
using max.stage.layer.template;

namespace max.vision.stage.component
{
    public sealed class MapCollision : CollisionComponent, ISightVisible
    {
        #region Properties

        public bool SightBlock { get { return true; } }

        public override Polygon2D Border { get { return Collision.Border; } }
        CollisionComponent Collision { get; set; }

        public override IStageLayer Parent 
        {
            get
            {
                return Collision.Parent;
            }
        }

        #endregion

        #region Constructors

        public MapCollision(CollisionComponent Collision)
        {
            this.Collision = Collision;
        }

        public MapCollision(Polygon2D Collision)
        {
            this.Collision = new CollisionComponent(Collision);
        }

        public MapCollision() : base() { }

        #endregion

        #region Controller

        public override void Initialize()
        {
            Collision.DoInitialize(Parent);
        }

        #endregion

        #region Geometry

        public override float DistanceToPoint(Vector2 Point)
        {
            return Collision.DistanceToPoint(Point);
        }

        public override bool LineIntersect(Line2D Line)
        {
            return Collision.LineIntersect(Line);
        }

        public override float LineIntersectDistance(Line2D Line)
        {
            return Collision.LineIntersectDistance(Line);
        }

        #endregion

        #region Sight

        public void Sight()
        {
            //clears entries
            MaxVision.MaxHud.ClearEntries();
            if (MaxVision.Options.Sound.SightCollisionSoundEnabled)
            {
                MaxVision.PlaySound(MaxVision.Options.Sound.SightCollisionFilename);
            }
        }

        #endregion

        #region Serialization
        protected override MaxSerializedObject DoSerialize(MaxSerializedObject Data)
        {
            Data = Collision.Serialize();
            return Data;
        }

        public override void Deserialize(MaxSerializedObject Data)
        {
            Collision = new CollisionComponent(Data);
        }

        #endregion

        #region CompareTo

        public override int CompareTo(object obj)
        {
            if (obj == null)
            {
                return 1;
            }
            GeometryComponent ob = (GeometryComponent)obj;
            if (ob != null)
            {
                float obdist = ob.DistanceToPoint(MaxVision.SoundController.Position);
                float dist = DistanceToPoint(MaxVision.SoundController.Position);

                if (obdist < dist)
                {
                    return 1;
                }
                else if (obdist > dist)
                {
                    return -1;
                }
                else if (obdist == dist)
                {
                    return 0;
                }
            }

            return 1;
        }

        #endregion
    }
}
