﻿using max.sound.path;
using max.vision.stage.component.label;

namespace max.vision.stage.path
{
    /// <summary>
    /// This class represents list of sound paths.
    /// 
    /// The sound paths are separated equally along the distance
    /// to give a steady stream of sounds travelling from the
    /// current location to the destination.
    /// </summary>
    public class AXSoundPathSequence : SoundPathSequence
    {
        public SoundPathSequence SoundPath { get; private set; }
        public AXLabel Label { get; private set; }

        public AXSoundPathSequence(SoundPathSequence SoundPathSequence, AXLabel Label)
        {
            SoundPath = SoundPathSequence;
            this.Label = Label;
        }

        public override void Dispose()
        {
            SoundPath.Dispose();
        }

        public override void Start()
        {
            SoundPath.Start();
        }

        public override void PauseMovement(){
            SoundPath.PauseMovement();
        }

    }
}