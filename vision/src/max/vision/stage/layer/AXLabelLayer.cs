﻿using max.serialize;
using max.stage;
using max.stage.component;
using max.stage.layer;
using max.stage.layer.template;
using max.vision.stage.component.label;
using System;
using System.Collections.Generic;

namespace max.vision.stage.layer
{
    public class AXLabelLayer : ComponentLayer
    {

        /// <summary>
        /// The items in the layer.
        /// </summary>
        public override List<ILayerComponent> Components
        {
            get
            {
                return new List<ILayerComponent>(Labels);
            }
            protected set
            {
                throw new NotImplementedException("This is intentionally unimplemented!");
            }
        }

        // the internal list of labels
        List<AXLabel> Labels = new List<AXLabel>();

        #region Constructors

        public AXLabelLayer(string ID, StageController StageController) : base(ID, StageController)
        {
            Persistent = true;
        }

        public AXLabelLayer() : base() { }

        #endregion

        #region Overriden methods
        public override void AddComponent(ILayerComponent Component)
        {
            Component.DoInitialize(this);
            if (Component is AXLabel)
            {
                Labels.Add((AXLabel)Component);
            }
        }

        public override void RemoveComponent(ILayerComponent Component)
        {
            Component.DoDispose();
            if (Component is AXLabel)
            {
                Labels.Remove((AXLabel)Component);
            }
        }

        /// <summary>
        /// Clears the layer's contents.
        /// </summary>
        public override void Clear()
        {
            for (int i = 0; i < Labels.Count; i++)
            {
                Labels[i].DoDispose();
            }
            Labels.Clear();
        }

        public override void Deserialize(MaxSerializedObject Data)
        {
            Clear();
            List<MaxSerializedObject> objs = Data.GetSerializedObjectCollection(SER_DATA);

            for (int i = 0; i < objs.Count; i++)
            {
                
                Labels.Add((AXLabel)objs[i].Deserialize());
            }
        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new AXLabelLayer();
            a.Deserialize(Data);
            return a;
        }
        #endregion
    }
}
