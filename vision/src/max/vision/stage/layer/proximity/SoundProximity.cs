﻿using max.geometry.shape2D;
using max.sound;
using max.sound.controller;
using max.sound.effect;
using max.sound.emitter;
using max.stage.component;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace max.vision.stage.layer.proximity
{

    /// <summary>
    /// This class represents a single proximity detector.
    /// A set number of proximity detectors surround the Sound Listener's position,
    /// based on the value in MaxSeerOptions.Sound.ProximityCount, in a circle.
    /// 
    /// The proximity detector will not cross past polygons.
    /// As a proximity detector approaches a polygon, it will make a sound, based on
    /// its angle and distance from the Sound Listener. 
    /// 
    /// When the proximity detector is inside a polygon, the sound's properties are not
    /// updated.
    /// </summary>
    /// 

    // Sound Proximity Engine originally inspired by work produced by Aaron Baker (http://vgstorm.com/)
    internal class SoundProximity
    {
        //if this is greater than the amount of proximity detectors
        //then it will not set any values
        static int InstanceCount = 0;
        bool Enabled;
        /// <summary>Angle of this sound.</summary>
        private float Angle;
        /// <summary>The angle represented as a Vector2 with origin (0,0,0).</summary>
        private Vector2 Direction
        {
            get
            {
                return
                new Vector2(
                (float)Math.Cos(listenerAngle) * MaxVision.Options.Sound.ProximityMaxDistance,
                (float)Math.Sin(listenerAngle) * MaxVision.Options.Sound.ProximityMaxDistance);
            }
        }

        private float listenerAngle { get { return Angle - SoundController.LookAngle; } }
        /// <summary>The current position of the proximity detector.</summary>
        public Vector2 Position { get; protected set; }

        public Vector2 ApparentPosition { get 
            {
                
                return Position; } 
        }
        /// <summary>The current pitch of the proximity detector.</summary>
        private float Pitch;
        /// <summary>The current pan of the proximity detector.</summary>
        private float Pan;

        static int DrawHeight = 5;
        static int DrawWidth = 5;

        /// <summary>The sound effect used for the sound proximity sound. Should be set when initializing the detectors in SoundController.</summary>
        MaxSoundInstance SoundEffect;

        SoundController SoundController { get { return Parent.SoundController; } }

        ProximityDetectorController Parent;

        Color col = Color.Red;

        float Distance;
        float LastDistance;

        int Cooldown = -1;
        int COOLDOWN_DEFAULT = 5;

        Vector2 PlayerAdjust;
        float PLAYER_ADJUST_RADIUS = 3;

        //debug property that should be removed later
        public bool DebugDisplayHallwayDetection { get; set; } = false;
        public SoundProximity(float a, ProximityDetectorController Parent, Color col)
        {
            if (MaxVision.Options.Sound.ProximitySoundEffect != null)
            {
                if (InstanceCount < MaxVision.Options.Sound.ProximityCount)
                {
                    this.Parent = Parent;
                    this.col = col;
                    InstanceCount++;
                    Enabled = true;
                    SoundEffect = MaxVision.Options.Sound.ProximitySoundEffect.Copy();
                    Angle = a;
                    PlayerAdjust = new Vector2((float)Math.Cos(a)* PLAYER_ADJUST_RADIUS, (float)Math.Sin(a)* PLAYER_ADJUST_RADIUS);
                    SetPitch();
                    SetPan();
                    SoundEffect.Pan = (Pan);
                    SoundEffect.Pitch = (Pitch);
                    SoundEffect.IsLooped = true;
                    SoundEffect.Play();
                }
            }
        }
        /// <summary>Sets the pitch of the detector based on the Direction variable.</summary>
        private void SetPitch()
        {
            Pitch = (float)Math.Cos(Math.Atan2(Direction.X, Direction.Y)) * MaxVision.Options.Sound.ProximityPitchMultiplier;
        }
        /// <summary>Sets the pan of the detector based on the Direction variable.</summary>
        private void SetPan()
        {
            Pan = (float)Math.Sin(Math.Atan2(Direction.X, Direction.Y));
        }

        /// <summary>Updates the proximity detector this cycle. Should be called every cycle.</summary>
        internal void Update()
        {
            if (Enabled)
            {
                if (SoundEffect != null)
                {
                    if (!MaxVision.Options.Sound.ProximityEnabled)
                    {
                        SoundEffect.Volume = 0;
                    }
                    else
                    {
                        LastDistance = Distance;
                        Distance = 0;
                        List<CollisionComponent> coll = MaxVision.MapController.Collisions;
                        if (coll.Count > 0)
                        {
                            Vector2 listen = Vector2.Add(MaxVision.SoundController.Position, PlayerAdjust);
                            Vector2 loc = Vector2.Add(listen, Direction);
                            float distvalue = 10000;

                            bool isInPolygon = false;

                            for (int i = 0; i < coll.Count; i++)
                            {
                                if (coll[i].Border.PointInGeometry( listen))
                                {
                                    isInPolygon = true;
                                    break;
                                }
                                distvalue = Math.Min(coll[i].LineIntersectDistance(new Line2D(listen, loc)), distvalue);
                            }

                            if (!isInPolygon)
                            {
                                Position = new Vector2(
                                    (float)Math.Cos(listenerAngle) * Math.Min(distvalue, MaxVision.Options.Sound.ProximityMaxDistance),
                                    (float)Math.Sin(listenerAngle) * Math.Min(distvalue, MaxVision.Options.Sound.ProximityMaxDistance));
                                if (distvalue < MaxVision.Options.Sound.ProximityMaxDistance)
                                {
                                    Distance = 1- (distvalue / MaxVision.Options.Sound.ProximityMaxDistance);

                                    if (SoundEffect.State == MaxSoundInstance.SoundState.STOPPED)
                                    {
                                        SoundEffect.Play();
                                    }
                                    SoundEffect.Volume = (((float)( Math.Pow(Distance, 2))) / MaxVision.Options.Sound.ProximityCount) * (float)(MaxVision.Options.Sound.ProximityVolume * .2);
                                }
                                else
                                {
                                    SoundEffect.Volume = 0;
                                }
                            }
                            else
                            {
                                SoundEffect.Volume = 1 * (float)(MaxVision.Options.Sound.ProximityVolume * .2);
                                Distance = 1;
                                Position = Vector2.Zero;
                            }
                        }
                        else
                        {
                            SoundEffect.Volume = 0;
                        }
                        if (MaxVision.Options.Sound.ProximityHallwayDetectionEnabled)
                        {
                            if (Cooldown == -1)
                            {
                                if (!ProximityDetectorController.CoolDown)
                                {
                                    if (LastDistance - Distance > MaxVision.Options.Sound.ProximityHallwayDetectionCutoff)
                                    {
                                        Cooldown = COOLDOWN_DEFAULT;

                                        MaxSoundInstance s = MaxVision.PlaySound(
                                                    MaxVision.Options.Sound.SoundHallwayDetectionFilename);
                                        if (s != null)
                                        {
                                            s.Pan = Pan;
                                            s.Pitch = Pitch;
                                        }

                                        ProximityDetectorController.CoolDown = true;
                                    }
                                }
                            }
                            else
                            {
                                Cooldown -= 1;
                                if (Cooldown == -1)
                                {
                                    ProximityDetectorController.CoolDown = false;
                                }
                            }
                        }
                    }
                }
            }
        }

        internal void UpdateInsideWall()
        {
            if (Enabled)
            {
                if (SoundEffect != null)
                {
                    if (!MaxVision.Options.Sound.ProximityEnabled)
                    {
                        SoundEffect.Volume = 0;
                    }
                    else
                    {
                        float finalDist = 1;
                        Position = MaxVision.PlayerPostion;
                        if (SoundEffect.State == MaxSoundInstance.SoundState.STOPPED)
                        {
                            SoundEffect.Play();
                        }
                        SoundEffect.Volume = (finalDist / MaxVision.Options.Sound.ProximityCount) * (float)(MaxVision.Options.Sound.ProximityVolume * .2);

                    }
                }
            }
        }

        //just disables the sound effect so that maxseer can shut the sound off.
        internal void Disable()
        {
            SoundEffect.Volume = 0;
        }

        internal void Dispose()
        {
            SoundEffect.Dispose();
        }

        public void Draw(SpriteBatch sb, GraphicsDevice gd, Vector2 position)
        {
            Vector2 pos = ApparentPosition;

                DrawDebug(
                DrawHeight,
                DrawWidth,
                pos + position,

                sb, gd, col);
        }

        private static void DrawDebug(int width, int height, Vector2 position, SpriteBatch spriteBatch, GraphicsDevice GD, Color c)
        {
            if (spriteBatch != null)
            {
                Color[] data = new Color[width * height];
                Texture2D rectTexture = new Texture2D(GD, width, height);

                for (int i = 0; i < data.Length; ++i)
                    data[i] = Color.White;

                rectTexture.SetData(data);

                position = new Vector2((float)(position.X - width * .5), (float)(position.Y - height * .5));

                spriteBatch.Draw(rectTexture, position, c);
            }

        }

    }
}
