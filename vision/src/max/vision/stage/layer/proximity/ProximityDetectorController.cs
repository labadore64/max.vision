﻿using max.serialize;
using max.sound;
using max.sound.controller;
using max.stage;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using stage.layer.controller;
using System;

namespace max.vision.stage.layer.proximity
{

    /// <summary>
    /// This class is a simple container for all the proximity detectors
    /// used in the game.
    /// </summary>
    public sealed class ProximityDetectorController : Controller
    {
        #region Properties

        internal static bool CoolDown { get; set; }
        internal SoundProximity[] ProximitySounds { get; set; } = new SoundProximity[0];

        /// <summary>
        /// The sound controller used for the vision engine.
        /// </summary>
        /// <value>Sound controller</value>
        public SoundController SoundController { get { return MaxVision.SoundController; } }

        /// <summary>
        /// Proximity Container is always persistent.
        /// </summary>
        /// <value>True</value>
        public override bool Persistent { 
            get { 
                return true; 
            } 
            set { } 
        }

        public override StageController StageController { get { return Parent.StageController; } set { } }

        #endregion

        public void DrawBoxes(SpriteBatch sb, GraphicsDevice gd, Vector2 position)
        {

            for (int i = 0; i < ProximitySounds.Length; i++)
            {
                ProximitySounds[i].Draw(sb, gd, position);
            }
        }

        #region Controller

        /// <summary>
        /// Initializes the Proximity Detector.
        /// </summary>
        /// <param name="StageLayer">Parent layer</param>
        public override void Initialize()
        {
            ProximitySounds = new SoundProximity[MaxVision.Options.Sound.ProximityCount];

            float anglemultiplier = (float)Math.PI * (2f / MaxVision.Options.Sound.ProximityCount);

            SoundProximity p;

            for (int i = 0; i < MaxVision.Options.Sound.ProximityCount; i++)
            {
                //Note, in this version its only a 2D vector using X and Y,
                //ignoring z. but I'm implementing it with vector3 so that
                //z could eventually be implemented easier.

                float adjust = (float)Math.PI;
                Color[] colors = new Color[8];
                colors[0] = Color.Red;
                colors[1] = Color.Orange;
                colors[2] = Color.Yellow;
                colors[3] = Color.Lime;
                colors[4] = Color.Cyan;
                colors[5] = Color.Blue;
                colors[6] = Color.Magenta;
                colors[7] = Color.Maroon;

                p = new SoundProximity((anglemultiplier * i) + adjust, this, colors[i]);
                ProximitySounds[i] = p;
            }
        }

        /// <summary>Updates the proximity detector.</summary>
        public override void Update()
        {
            if (!Disposed)
            {
                for (int i = 0; i < ProximitySounds.Length; i++)
                {
                    ProximitySounds[i].Update();
                }
            }
        }

        /// <summary>
        /// Disposes of the proximity detector's resources.
        /// </summary>
        public override void Dispose()
        {
            for(int i = 0; i < ProximitySounds.Length; i++)
            {
                ProximitySounds[i].Dispose();
            }
            Disposed = true;
        }

        #endregion

        #region Serialize

        public override MaxSerializedObject Serialize()
        {
            // todo: serialize properties here
            return null;
        }

        public override void Deserialize(MaxSerializedObject Data)
        {
            // todo: deserialize properties here
        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new ProximityDetectorController();
            a.Deserialize(Data);
            return a;
        }

        #endregion
    }
}
