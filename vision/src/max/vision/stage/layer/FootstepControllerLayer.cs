﻿using System.Collections.Generic;
using max.serialize;
using max.sound.effect;
using max.stage;
using max.stage.component;
using max.stage.layer.template;
using Microsoft.Xna.Framework;

namespace max.vision.stage.layer
{

    /// <summary>
    /// <para>
    /// Plays a footstep sound when you walk around.
    /// Note that this features is built for accessibility purposes, not animation purposes.
    /// You may want to build your own footstep controller for that, and only enable this when accessibility mode is enabled.
    /// </para>
    /// <para>
    /// When the player traverses a set distance, it will play a footstep sound.
    /// This allows blind players to tell approximately how far they have travelled.
    /// </para>
    /// </summary>
    /// 

    public class FootstepControllerLayer : ControllerLayer
    {

        #region Properties

        public override List<ILayerComponent> Components {
            get
            {
                return new List<ILayerComponent>(new ILayerComponent[] { this });
            }
            protected set { } 
        }

        /// <summary>
        /// The sound that is used for the footstep sound.
        /// </summary>
        public MaxSoundInstance StepSound
        {
            get
            {
                return MaxVision.Options.Sound.FootstepSoundEffect;
            }
            set
            {
                MaxVision.Options.Sound.FootstepSoundEffect = value;
            }
        }
        ///<summary>
        /// The distance that should be travelled before triggering the sound
        /// </summary>
        public float TriggerDistance
        {
            get
            {
                return MaxVision.Options.Sound.FootstepStepDistance;
            }
            set
            {
                MaxVision.Options.Sound.FootstepStepDistance = value;
            }
        }

        /// <summary>
        /// How much distance has been travelled since the last sound triggered.
        /// </summary>
        private float DistanceTravelled;
        /// <summary>
        /// Controls alternating pitch change. Switches every time that the sound is made.
        /// </summary>
        private bool PitchChange;

        /// <summary>
        /// Changes how much the pitch adjusts with the alternate step.
        /// </summary>
        private float PitchAmount
        {
            get
            {
                return MaxVision.Options.Sound.FootstepPitchChange;
            }
            set
            {
                MaxVision.Options.Sound.FootstepPitchChange = value;
            }
        }
        /// <summary>
        /// Whether or not the pitch change should be activated.
        /// </summary>
        private bool PitchChangeActive
        {
            get
            {
                return MaxVision.Options.Sound.FootstepPitchChangeActive;
            }
            set
            {
                MaxVision.Options.Sound.FootstepPitchChangeActive = value;
            }
        }

        #endregion

        #region Constructor
        public FootstepControllerLayer(string ID, StageController StageController) : base(ID, StageController)
        {
            this.ID = ID;
            this.StageController = StageController;
        }

        public FootstepControllerLayer() : base()
        {

        }
        #endregion

        #region Controller
        /// <summary>
        /// Updates the footstep controller. Should be called once every cycle.
        /// </summary>
        public override void Update()
        {
            if (MaxVision.Options.Sound.FootstepEnabled && StepSound != null)
            {
                DistanceTravelled += Vector2.Distance(MaxVision.SoundController.Position, MaxVision.SoundController.LastPosition);

                if (DistanceTravelled > TriggerDistance)
                {

                    //sets the distance travelled to itself mod the trigger distance
                    DistanceTravelled %= TriggerDistance;
                    StepSound.IsLooped = false;

                    //if pitch changing is on, do the thing here
                    if (PitchChangeActive)
                    {
                        if (PitchChange)
                        {
                            StepSound.Pitch = PitchAmount;
                        }
                        else
                        {
                            StepSound.Pitch = (1);
                        }
                        PitchChange = !PitchChange;
                    }

                    StepSound.Play();
                }
            }
        }

        #endregion

        #region Serialize
        /// <summary>
        /// Serialize the layer into a dictionary.
        /// </summary>
        /// <returns>Serialized data</returns>
        protected override MaxSerializedObject DoSerialize(MaxSerializedObject Data)
        {
            // Add stuff here.
            // Since this one doesn't actually use a controller it will throw an error otherwise
            return Data;
        }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new FootstepControllerLayer();
            a.Deserialize(Data);
            return a;
        }
        #endregion

    }
}
