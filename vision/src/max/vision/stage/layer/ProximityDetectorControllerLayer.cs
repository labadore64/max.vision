﻿using max.serialize;
using max.stage;
using max.stage.layer.template;
using max.vision.stage.layer.proximity;

namespace max.vision.stage.layer
{
    public sealed class ProximityDetectorControllerLayer : ControllerLayer
    {
        public ProximityDetectorControllerLayer(string ID, StageController StageController) : base(ID, StageController)
        {
            this.ID = ID;
            this.StageController = StageController;
            Controller = new ProximityDetectorController();
        }

        public ProximityDetectorControllerLayer() : base() { }

        /// <summary>
        /// Creates an instance of this type of object from the provided data.
        /// </summary>
        /// <param name="Data">Data</param>
        /// <returns>Deserialized new instance</returns>
        public override IMaxSerializable CreateInstance(MaxSerializedObject Data)
        {
            IMaxSerializable a = new ProximityDetectorControllerLayer();
            a.Deserialize(Data);
            return a;
        }
    }
}
