﻿namespace max.vision.stage
{
 
    /// <summary>
    /// An interface for objects that can be seen by the vision engine
    /// </summary>
    public interface ISightVisible
    {
        #region Properties
        /// <summary>
        /// Whether or not this object blocks sight.
        /// </summary>
        bool SightBlock { get; }
        #endregion

        #region Control methods

        /// <summary>
        /// Action when this object is seen.
        /// </summary>
        void Sight();

        #endregion
    }
}
