﻿using max.geometry;
using max.geometry.pathfinder;
using max.stage;
using max.stage.component;
using max.stage.layer;
using max.stage.layer.template;
using max.vision.stage.component;
using max.vision.stage.component.label;
using max.vision.stage.layer;
using max.vision.stage.path;
using max.vision.stage.layer.proximity;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using max.serialize;
using max.IO;
using max.geometry.shape2D;

namespace max.vision.stage
{
    public sealed class AXMapController
    {
        StageController StageController = new StageController();

        #region Constants
        public const string LAYER_ID_MAIN = "max.vision.ax.ROOT";
        public const string LAYER_ID_COLLISION = "max.vision.ax.COLLISION_LAYER";
        public const string LAYER_ID_LABEL = "max.vision.ax.LABEL_LAYER";
        public const string LAYER_ID_BOOKMARK = "max.vision.ax.BOOKMARK_LAYER";
        public const string LAYER_ID_CONTROLLER_PROX = "max.vision.ax.CONTROLLER_PROX_DETECT";
        public const string LAYER_ID_CONTROLLER_FOOTSTEP = "max.vision.ax.CONTROLLER_FOOTSTEP";

        public const string LAYER_ID_PLAYER = "player";
        #endregion

        #region Properties

        public List<CollisionComponent> Collisions
        {
            get
            {
                return CollisionLayer.Collisions;
            }
        }

        /// <summary>
        /// The path leading from one point to another.
        /// </summary>
        /// <value>Sound path sequence</value>
        public AXSoundPathSequence LocatorPath { get; private set; } = null;

        public CollisionLayer CollisionLayer { get; private set; }

        public AXLabelLayer BookmarkLayer { get; private set; }

        public AXLabelLayer LabelLayer { get; private set; }

        public ProximityDetectorControllerLayer ProximityDetector { get; private set; }

        public FootstepControllerLayer FootstepController { get; private set; }

        public Player2DLayer PlayerLayer { get; private set; }

        public ISightVisible LookedObject { get; private set; } = null;
        public ISightVisible LastLookedObject { get; private set; } = null;

        /// <summary>The container for proximity detectors.</summary>
        public ProximityDetectorController ProximityDetectors
        {
            get
            {
                return (ProximityDetectorController)ProximityDetector.Controller;
            }
        }

        /// <summary>
        /// The pathfinder
        /// </summary>
        public static MapPathFinder PathFinder { get; private set; } = null;

        public AXSoundPathSequence NavigationPath { get; private set; } = null;

        /// <summary>
        /// The list of layers that are visible to the max.vision engine.
        /// </summary>
        public List<IStageLayer> SightLayers { get; private set; } = new List<IStageLayer>();

        #endregion

        #region Constructors

        public AXMapController(StageController StageController)
        {
            this.StageController = StageController;
        }

        #endregion

        #region Controller
        public void Initialize()
        {
            StageController.Initialize();
            StageController.SoundController = MaxVision.SoundController;
            // Add player layer
            StageController.AddLayer(new Player2DLayer(LAYER_ID_PLAYER, StageController));
            PlayerLayer = (Player2DLayer)StageController.GetLayer(LAYER_ID_PLAYER);

            //initialize the main layer.
            StageController.AddLayer(new ContainerLayer(LAYER_ID_MAIN,StageController));
            ContainerLayer Layers = (ContainerLayer)StageController.GetLayer(LAYER_ID_MAIN);

            // component layers
            Layers.AddLayer(new CollisionLayer(LAYER_ID_COLLISION,StageController));
            Layers.AddLayer(new AXLabelLayer(LAYER_ID_LABEL,StageController));
            Layers.AddLayer(new AXLabelLayer(LAYER_ID_BOOKMARK, StageController));

            // controller layers
            Layers.AddLayer(new ProximityDetectorControllerLayer(LAYER_ID_CONTROLLER_PROX, StageController));
            Layers.AddLayer(new FootstepControllerLayer(LAYER_ID_CONTROLLER_FOOTSTEP, StageController));

            // sets the layers to variables as well so that seek times are faster
            CollisionLayer = (CollisionLayer)Layers.GetLayer(LAYER_ID_COLLISION);
            // set the collision layer's persistence to true or else it will die
            CollisionLayer.Persistent = true;

            BookmarkLayer = (AXLabelLayer)Layers.GetLayer(LAYER_ID_BOOKMARK);
            LabelLayer = (AXLabelLayer)Layers.GetLayer(LAYER_ID_LABEL);
            ProximityDetector = (ProximityDetectorControllerLayer)Layers.GetLayer(LAYER_ID_CONTROLLER_PROX);
            FootstepController = (FootstepControllerLayer)Layers.GetLayer(LAYER_ID_CONTROLLER_FOOTSTEP);

            // done to fix a bug
            ProximityDetector.DoUpdate();

            // set sight layers

            AddSightLayer(CollisionLayer);
            AddSightLayer(LabelLayer);
      
        }

        public void Update()
        {
            StageController.Update();
            DoSight();
        }

        public void ClearMap()
        {
            ClearLabels();
            ClearBookmarks();
            ClearCollisions();
            StageController.SoundController.ClearEntities();
        }
        #endregion

        #region Map Management

        #region Labels

        public void ClearLabels()
        {
            LabelLayer.Clear();
        }

        public List<AXLabel> GetLabels()
        {
            AXLabelLayer layer = LabelLayer;

            List<AXLabel> list =  new List<AXLabel>();

            for(int i = 0; i < layer.Components.Count; i++)
            {
                list.Add((AXLabel)layer.Components[i]);
            }

            return list;
        }

        public AXLabel GetLabelFromName(string Name)
        {
            return GetLabels().Find(x => x.Name == Name);
        }

        public AXLabel AddLabel(AXLabel Label)
        {
            LabelLayer.AddComponent(Label);
            return Label;
        }

        public AXLabel RemoveLabel(AXLabel Label)
        {
            LabelLayer.RemoveComponent(Label);
            return Label;
        }

        #endregion

        #region Collisions

        public void ClearCollisions()
        {
            CollisionLayer.Clear();
            MaxVision.SoundController.Collisions.Clear();
        }

        public MapCollision AddCollision(MapCollision Collision)
        {
            CollisionLayer.AddComponent(Collision);
            // remove this later
            PathFinder = new MapPathFinder(CollisionLayer.Polygons);
            MaxVision.SoundController.Collisions.Add(Collision.Border);
            return Collision;
        }


        public MapCollision RemoveCollision(MapCollision Collision)
        {
            CollisionLayer.RemoveComponent(Collision);
            // remove this later
            PathFinder = new MapPathFinder(CollisionLayer.Polygons);
            MaxVision.SoundController.Collisions.Remove(Collision.Border);
            return Collision;
        }

        public List<CollisionComponent> GetCollisions(Vector2 Point)
        {
            return CollisionLayer.GetCollisions(Point);
        }

        public List<CollisionComponent> GetCollisions(I2DGeometry Geometry)
        {
            return CollisionLayer.GetCollisions(Geometry);
        }

        #endregion

        #region Bookmarks
        public BookmarkLabel AddBookmark(BookmarkLabel Bookmark)
        {
            BookmarkLayer.AddComponent(Bookmark);
            return Bookmark;
        }

        public BookmarkLabel RemoveBookmark(BookmarkLabel Bookmark)
        {
            BookmarkLayer.RemoveComponent(Bookmark);
            return Bookmark;
        }

        public List<BookmarkLabel> GetBookmarks()
        {
            AXLabelLayer layer = BookmarkLayer;

            List<BookmarkLabel> list = new List<BookmarkLabel>();

            for (int i = 0; i < layer.Components.Count; i++)
            {
                list.Add((BookmarkLabel)layer.Components[i]);
            }

            return list;
        }

        public BookmarkLabel GetBookmarkFromName(string Name)
        {
            return GetBookmarks().Find(x => x.Name == Name);
        }

        public void ClearBookmarks()
        {
            BookmarkLayer.Clear();
        }
        #endregion

        #region SoundPath
        public AXSoundPathSequence GenerateLocatorPath(Vector2 Position, AXLabel AXLabel)
        {
            if (NavigationPath != null)
            {
                MaxVision.SoundController.RemoveSoundPathSequence(NavigationPath.SoundPath);
            }
            NavigationPath = new AXSoundPathSequence(
                MaxVision.SoundController.CreateSoundPathSequence(
                    PathFinder.GetPathBetweenPoints(Position, AXLabel.Position),
                    MaxVision.Options.Sound.SoundPathSoundFilename,
                    MaxVision.Options.Sound.SoundPathSpeed
                    ),
                AXLabel);
            NavigationPath.Start();
            return NavigationPath;
        }

        public void ClearLocatorPath()
        {
            if (LocatorPath != null)
            {
                LocatorPath.Dispose();
            }
            LocatorPath = null;
        }

        #endregion

        #region Sight

        /// <summary>
        /// Adds a layer that interacts with max.vision
        /// </summary>
        /// <param name="Layer">Layer to add</param>
        public void AddSightLayer(IStageLayer Layer)
        {
            //checks if sight layer already exists in list
            if (!SightLayers.Contains(Layer))
            {
                SightLayers.Add(Layer);
            }
        }

        /// <summary>
        /// Removes a layer that interacts with max.vision
        /// </summary>
        /// <param name="Layer">Layer to add</param>
        public void RemoveSightLayer(IStageLayer Layer)
        {
            SightLayers.Remove(Layer);
        }

        private void DoSight()
        {
            if (MaxVision.Options.Map.SightEnabled)
            {
                //Checks if the position of the listener changed this frame
                if (MaxVision.SoundController.MovedThisFrame || MaxVision.SoundController.ReleasedThisFrame)
                {
                    ISightVisible lookedComponent = null;

                    List<GeometryComponent> components = new List<GeometryComponent>();
                    
                    for(int i = 0; i < SightLayers.Count; i++)
                    {
                        for (int j = 0; j < SightLayers[i].Components.Count; j++)
                        {
                            components.Add((GeometryComponent)SightLayers[i].Components[j]);
                        }
                    }


                    for (int i = 0; i < components.Count; i++)
                    {
                        if (!components[i].LineIntersect(MaxVision.SoundController.LookLine))
                        {
                            components.Remove(components[i]);
                            i--;
                        }
                    }

                    components.Sort((x, y) => x.CompareTo(y));


                    if (components.Count > 0)
                    {
                        lookedComponent = (ISightVisible)components[0];

                        if (components.Count > 1)
                        {

                            if (((ISightVisible)components[0]).SightBlock)
                            {
                                if (!((ISightVisible)components[1]).SightBlock &&
                                    Math.Abs(components[1].DistanceToPoint(MaxVision.SoundController.Position)
                                    - components[0].DistanceToPoint(MaxVision.SoundController.Position)) 
                                    < MaxVision.Options.Sound.SightSeeThroughDistance)
                                {
                                    lookedComponent = (ISightVisible)components[1];
                                }
                            }
                        }
                    }



                    if (lookedComponent != LookedObject || MaxVision.SoundController.ReleasedThisFrame)
                    {
                        LastLookedObject = LookedObject;
                        LookedObject = lookedComponent;
                        if (LookedObject != null)
                        {
                            LookedObject.Sight();
                        }
                        else
                        {
                            MaxVision.MaxHud.ClearEntries();
                        }

                    }
                }
            }
        }

        #endregion

        #region Loading Map

        public void WriteLayers()
        {
            String holder = MaxSerializer.SerializeYAML(StageController.Serialize());
            TextFileWriter.WriteFile("config\\input", "test", ".txt", holder);
        }

        public void ReadLayers()
        {
            String holder = TextFileReader.ReadFile("config\\input", "test", ".txt");
            StageController.Deserialize(MaxSerializer.DeserializeYAML(holder));
        }

        #endregion

        #endregion
    }
}
