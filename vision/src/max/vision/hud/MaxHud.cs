﻿using max.input;
using System;
using System.Collections.Generic;

namespace max.vision.hud
{
    /// <summary>
    /// Represents an accessible HUD. 
    /// To use, bind a HUD display value to a name. Then, you can read it out at will.
    /// A good design implementation would be adding accessible key inputs to read off HUD data,
    /// then use this class to just read that data.
    /// Keep in mind that accessibility strings should be as short as possible, with the
    /// variable information first. Like this: "30 out of 50 HP" instead of "You have 30 health out of 50 left."
    /// </summary>
    public class MaxHud
    {
        Dictionary<string, string> Entries = new Dictionary<string, string>();
        public string GetEntry(string ID)
        {
            if (Entries.ContainsKey(ID))
            {
                return Entries[ID];
            }

            return "";
        }

        public MaxHud()
        {

        }

        public void SetEntry(string ID, string Value)
        {
            if (Entries.ContainsKey(ID))
            {
                Entries[ID] = Value;
            }
            else
            {
                Entries.Add(ID, Value);
            }
        }

        public void SetEntries(Dictionary<string,string> EntryValues)
        {
            Entries = EntryValues;
        }

        public void ClearEntries()
        {
            Entries.Clear();
        }

        public void Update()
        {
            for(int i = 0; i < InputTypes.Access.Length; i++) {
                if (Entries.ContainsKey(InputTypes.Access[i]))
                {
                    if (MaxVision.InputController.TestAccessPressed(i))
                    {
                        MaxVision.Speak(Entries[InputTypes.Access[i]]);
                    }
                }
            }
        }

    }
}
